package com.olekdia.datetimepickers;

import java.util.Calendar;

public interface ICommonDatePickerController {

    int YEAR_VIEW = 1;

    int ANIMATION_DURATION = 300;
    int ANIMATION_DELAY = 500;

    void onYearSelected(int year);

    int getSelectedYear();

    int getNumeralSystem();

    void registerOnDateChangedListener(IOnDateChangedListener listener);

    void unregisterOnDateChangedListener(IOnDateChangedListener listener);

    boolean isThemeDark();

    int getAccentColor();

    int getPrimaryColor();

    int getMinYear();

    int getMaxYear();

    Calendar getStartDate();

    Calendar getEndDate();

    void tryVibrate();
}
