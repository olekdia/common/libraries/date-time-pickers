/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.olekdia.datetimepickers;

public interface IKeyScheme {

    // Commons
    String KEY_TITLE = "title";
    String KEY_THEME_DARK = "theme_dark";
    String KEY_THEME_DARK_CHANGED = "theme_dark_changed";
    String KEY_ACCENT = "accent";
    String KEY_PRIMARY = "primary";
    String KEY_VIBRATE = "vibrate";
    String KEY_DISMISS = "dismiss";
    String KEY_OK_RESID = "ok_resid";
    String KEY_OK_STRING = "ok_string";
    String KEY_CANCEL_RESID = "cancel_resid";
    String KEY_CANCEL_STRING = "cancel_string";
    String KEY_NUMERAL_SYSTEM = "numeral_system";

    // Time
    String KEY_INITIAL_TIME = "initial_time";
    String KEY_IS_24_HOUR_VIEW = "is_24_hour_view";
    String KEY_CURRENT_ITEM_SHOWING = "current_item_showing";
    String KEY_SELECTABLE_TIMES = "selectable_times";
    String KEY_MIN_TIME = "min_time";
    String KEY_MAX_TIME = "max_time";
    String KEY_ENABLE_SECONDS = "enable_seconds";
    String KEY_AM = "am";
    String KEY_PM = "pm";

    // Date
    String KEY_SELECTED_YEAR = "year";
    String KEY_SELECTED_MONTH = "month";
    String KEY_SELECTED_DAY = "day";
    String KEY_LIST_POSITION = "list_position";
    String KEY_WEEK_START = "week_start";
    String KEY_DAYS_OF_WEEK = "days_of_week";
    String KEY_YEAR_START = "year_start";
    String KEY_YEAR_END = "year_end";
    String KEY_CURRENT_VIEW = "current_view";
    String KEY_LIST_POSITION_OFFSET = "list_position_offset";
    String KEY_MIN_DATE = "min_date";
    String KEY_MAX_DATE = "max_date";
    String KEY_HIGHLIGHTED_DAYS = "highlighted_days";
    String KEY_SELECTABLE_DAYS = "selectable_days";
    String KEY_AUTO_DISMISS = "auto_dismiss";
    String KEY_DEFAULT_VIEW = "default_view";
}
