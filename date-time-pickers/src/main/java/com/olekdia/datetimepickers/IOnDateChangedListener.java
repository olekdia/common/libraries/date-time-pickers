package com.olekdia.datetimepickers;

/**
 * The callback used to notify other date picker components of a change in selected date.
 */
public interface IOnDateChangedListener {

    void onDateChanged();

}