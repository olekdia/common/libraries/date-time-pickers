package com.olekdia.datetimepickers;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;

import com.olekdia.commonhelpers.ColorHelper;
import com.olekdia.commonhelpers.CommonHelper;

public class PickerDialog extends DialogFragment implements IKeyScheme {

    protected DialogInterface.OnCancelListener mOnCancelListener;
    protected DialogInterface.OnDismissListener mOnDismissListener;

    protected HapticFeedbackController mHapticFeedbackController;

    protected int mNumeralSystem = CommonHelper.WESTERN_ARABIC;
    protected boolean mThemeDark = false;
    protected boolean mThemeDarkChanged = false;
    protected int mAccentColor = CommonHelper.INVALID;
    protected int mPrimaryColor = CommonHelper.INVALID;
    protected boolean mVibrate = true;
    protected boolean mDismissOnPause = false;
    protected boolean mAutoDismiss = false;

    protected String mTitle;
    protected int mOkResId = R.string.mdtp_ok;
    protected String mOkString;
    protected int mCancelResId = R.string.mdtp_cancel;
    protected String mCancelString;

    public PickerDialog() {
    }

//--------------------------------------------------------------------------------------------------
//  Lifecycle
//--------------------------------------------------------------------------------------------------

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_THEME_DARK, mThemeDark);
        outState.putBoolean(KEY_THEME_DARK_CHANGED, mThemeDarkChanged);
        outState.putInt(KEY_ACCENT, mAccentColor);
        outState.putInt(KEY_PRIMARY, mPrimaryColor);
        outState.putBoolean(KEY_VIBRATE, mVibrate);
        outState.putBoolean(KEY_DISMISS, mDismissOnPause);
        outState.putBoolean(KEY_AUTO_DISMISS, mAutoDismiss);
        outState.putString(KEY_TITLE, mTitle);
        outState.putInt(KEY_OK_RESID, mOkResId);
        outState.putString(KEY_OK_STRING, mOkString);
        outState.putInt(KEY_CANCEL_RESID, mCancelResId);
        outState.putString(KEY_CANCEL_STRING, mCancelString);
        outState.putInt(KEY_NUMERAL_SYSTEM, mNumeralSystem);
    }

    protected void restoreInstanceState(final Bundle savedInstanceState) {
        mThemeDark = savedInstanceState.getBoolean(KEY_THEME_DARK);
        mThemeDarkChanged = savedInstanceState.getBoolean(KEY_THEME_DARK_CHANGED);
        mAccentColor = savedInstanceState.getInt(KEY_ACCENT);
        mPrimaryColor = savedInstanceState.getInt(KEY_PRIMARY);
        mVibrate = savedInstanceState.getBoolean(KEY_VIBRATE);
        mDismissOnPause = savedInstanceState.getBoolean(KEY_DISMISS);
        mAutoDismiss = savedInstanceState.getBoolean(KEY_AUTO_DISMISS);
        mTitle = savedInstanceState.getString(KEY_TITLE);
        mOkResId = savedInstanceState.getInt(KEY_OK_RESID);
        mOkString = savedInstanceState.getString(KEY_OK_STRING);
        mCancelResId = savedInstanceState.getInt(KEY_CANCEL_RESID);
        mCancelString = savedInstanceState.getString(KEY_CANCEL_STRING);
        mNumeralSystem = savedInstanceState.getInt(KEY_NUMERAL_SYSTEM);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHapticFeedbackController.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        mHapticFeedbackController.stop();
        if (mDismissOnPause) dismiss();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (mOnCancelListener != null) mOnCancelListener.onCancel(dialog);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) mOnDismissListener.onDismiss(dialog);
    }

//--------------------------------------------------------------------------------------------------
//  Getters & setters
//--------------------------------------------------------------------------------------------------

    public final void setNumeralSystem(final @CommonHelper.NumeralSystem int system) {
        mNumeralSystem = system;
    }

    public int getNumeralSystem() {
        return mNumeralSystem;
    }

    /**
     * Set whether the device should setVibrate when touching fields
     *
     * @param vibrate true if the device should setVibrate when touching a field
     */
    public final void setVibrate(final boolean vibrate) {
        mVibrate = vibrate;
    }

    public void tryVibrate() {
        if (mVibrate) mHapticFeedbackController.tryVibrate();
    }

    /**
     * Set whether the picker should dismiss itself when a day is selected
     *
     * @param autoDismiss true if the dialog should dismiss itself when a day is selected
     */
    @SuppressWarnings("unused")
    public final void autoDismiss(final boolean autoDismiss) {
        mAutoDismiss = autoDismiss;
    }

    /**
     * Set whether the picker should dismiss itself when being paused or whether it should try to survive an orientation change
     *
     * @param dismissOnPause true if the dialog should dismiss itself when it's pausing
     */
    public final void dismissOnPause(final boolean dismissOnPause) {
        mDismissOnPause = dismissOnPause;
    }

    public final void setThemeDark(boolean themeDark) {
        mThemeDark = themeDark;
        mThemeDarkChanged = true;
    }

    public boolean isThemeDark() {
        return mThemeDark;
    }

    /**
     * Set the accent color of this dialog
     *
     * @param color the accent color you want
     */
    public final void setAccentColor(final @ColorInt int color) {
        mAccentColor = ColorHelper.replaceAlpha(color, 1F);
    }

    public int getAccentColor() {
        return mAccentColor;
    }

    public int getPrimaryColor() {
        return mPrimaryColor;
    }

    /**
     * Set a title. NOTE: this will only take effect with the next onCreateView
     */
    public final void setTitle(String title) {
        mTitle = title;
    }

    public final String getTitle() {
        return mTitle;
    }

    /**
     * Set the label for the Ok button (max 12 characters)
     */
    @SuppressWarnings("unused")
    public void setOkText(final String okString) {
        mOkString = okString;
    }

    /**
     * Set the label for the Ok button (max 12 characters)
     */
    @SuppressWarnings("unused")
    public final void setOkText(final @StringRes int resId) {
        mOkString = null;
        mOkResId = resId;
    }

    /**
     * Set the label for the Cancel button (max 12 characters)
     */
    @SuppressWarnings("unused")
    public final void setCancelText(final String cancelString) {
        mCancelString = cancelString;
    }

    /**
     * Set the label for the Cancel button (max 12 characters)
     */
    @SuppressWarnings("unused")
    public final void setCancelText(final @StringRes int resId) {
        mCancelString = null;
        mCancelResId = resId;
    }

    @SuppressWarnings("unused")
    public final void setOnCancelListener(final DialogInterface.OnCancelListener onCancelListener) {
        mOnCancelListener = onCancelListener;
    }

    @SuppressWarnings("unused")
    public final void setOnDismissListener(final DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }
}