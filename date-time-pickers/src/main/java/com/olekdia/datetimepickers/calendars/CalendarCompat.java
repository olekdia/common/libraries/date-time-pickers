package com.olekdia.datetimepickers.calendars;

import com.olekdia.commonhelpers.CommonHelper;

import java.util.Calendar;
import java.util.Locale;

public class CalendarCompat {

    public static Calendar getInstance() {
        switch (Locale.getDefault().getLanguage()) {
            case CommonHelper.FA_LANG:
                return new PersianCalendar();

            default:
                return Calendar.getInstance();
        }
    }
}