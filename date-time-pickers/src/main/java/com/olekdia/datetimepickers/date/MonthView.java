/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.datetimepickers.date;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.olekdia.commonhelpers.CommonHelper;
import com.olekdia.datetimepickers.DTPickersHelper;
import com.olekdia.datetimepickers.R;
import com.olekdia.datetimepickers.calendars.CalendarCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * A calendar-like view displaying a specified month and the appropriate selectable day numbers
 * within the specified month.
 */
public abstract class MonthView extends View {

    protected static int DEFAULT_HEIGHT = 32;
    protected static int MIN_HEIGHT = 10;
    protected static final int DEFAULT_NUM_ROWS = 6;
    protected static final int MAX_NUM_ROWS = 6;

    private static final int SELECTED_CIRCLE_ALPHA = 255;

    protected final int mDaySeparatorWidth = 1;
    protected final int mMiniDayNumberTextSize;
    protected final int mMonthLabelTextSize;
    protected final int mMonthDayLabelTextSize;
    protected final int mMonthHeaderSize;
    protected final int mDaySelectedCircleSize;

    protected IDatePickerController mController;

    // affects the padding on the sides of this view
    protected int mEdgePadding = 0;

    protected Typeface mDefNormalTypeface;
    protected Typeface mDefBoldTypeface;

    protected Paint mMonthNumPaint;
    protected Paint mMonthTitlePaint;
    protected Paint mSelectedCirclePaint;
    protected Paint mMonthDayLabelPaint;

    protected int mMonth;

    protected int mYear;
    // Quick reference to the width of this view, matches parent
    protected int mWidth;
    // The height this view should draw at in pixels, set by height param
    protected int mRowHeight = DEFAULT_HEIGHT;
    // If this view contains the today
    protected boolean mHasToday = false;
    // Which day is selected [0-6] or -1 if no day is selected
    protected int mSelectedDay = -1;
    // Which day is today [0-6] or -1 if no day is today
    protected int mToday = -1;
    // Which day of the week to start on [0-6]
    protected int mWeekStart = Calendar.SUNDAY;
    // The number of days + a spot for week number if it is displayed
    protected int mNumCells = CommonHelper.DAYS_IN_WEEK;
    private int mDayOfWeekStart = 0;
    private final boolean mIsRtl;

    private final Calendar mCalendar;

    protected int mNumRows = DEFAULT_NUM_ROWS;

    // Optional listener for handling day click actions
    protected OnDayClickListener mOnDayClickListener;

    protected int mPrimaryTextColor;
    protected int mSelectedDayTextColor;
    protected int mSecondaryTextColor;
    protected int mHighlightedDayTextColor;
    protected int mDisabledDayTextColor;
    protected int mMonthTitleColor;
    private SimpleDateFormat mFormatter;

    public MonthView(Context context) {
        this(context, null, null);
    }

    public MonthView(Context context, AttributeSet attr, IDatePickerController controller) {
        super(context, attr);
        mController = controller;
        final Resources res = context.getResources();

        mCalendar = CalendarCompat.getInstance();

        mDefNormalTypeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL);
        mDefBoldTypeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD);

        mIsRtl = CommonHelper.isLayoutRtl(res);
        final String pattern = CommonHelper.getBestDateTimePattern(Locale.getDefault(), "LLLL yyyy");

        mFormatter = new SimpleDateFormat(pattern, Locale.getDefault());
        mFormatter.applyLocalizedPattern(pattern);

        final boolean darkTheme = mController != null && mController.isThemeDark();
        if (darkTheme) {
            mPrimaryTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_normal_dark_theme);
            mSecondaryTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_month_day_dark_theme);
            mDisabledDayTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_disabled_dark_theme);
            mHighlightedDayTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_highlighted_dark_theme);
        } else {
            mPrimaryTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_normal);
            mSecondaryTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_month_day);
            mDisabledDayTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_disabled);
            mHighlightedDayTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_highlighted);
        }
        mSelectedDayTextColor = ContextCompat.getColor(context, R.color.mdtp_white);
        mMonthTitleColor = ContextCompat.getColor(context, R.color.mdtp_white);

        mMiniDayNumberTextSize = res.getDimensionPixelSize(R.dimen.mdtp_day_number_size);
        mMonthLabelTextSize = res.getDimensionPixelSize(R.dimen.mdtp_month_label_size);
        mMonthDayLabelTextSize = res.getDimensionPixelSize(R.dimen.mdtp_month_day_label_text_size);
        mMonthHeaderSize = res.getDimensionPixelOffset(R.dimen.mdtp_month_list_item_header_height);
        mDaySelectedCircleSize = res
                .getDimensionPixelSize(R.dimen.mdtp_day_number_select_circle_radius);

        mRowHeight = (res.getDimensionPixelOffset(R.dimen.mdtp_date_picker_view_animator_height)
                - getMonthHeaderSize()) / MAX_NUM_ROWS;

        // Sets up any standard paints that will be used
        initView();
    }

    public void setDatePickerController(IDatePickerController controller) {
        mController = controller;
    }

    public void setOnDayClickListener(OnDayClickListener listener) {
        mOnDayClickListener = listener;
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                final int day = getDayFromLocation(event.getX(), event.getY());
                if (day >= 0) {
                    onDayClick(day);
                }
                break;
        }
        return true;
    }

    /**
     * Sets up the text and style properties for painting. Override this if you
     * want to use a different paint.
     */
    protected void initView() {
        final Typeface boldFont = Typeface.create(getContext().getResources().getString(R.string.mdtp_sans_serif), Typeface.BOLD);
        mMonthTitlePaint = new Paint();
        mMonthTitlePaint.setFakeBoldText(true);
        mMonthTitlePaint.setAntiAlias(true);
        mMonthTitlePaint.setTextSize(mMonthLabelTextSize);
        mMonthTitlePaint.setTypeface(boldFont);
        mMonthTitlePaint.setColor(mPrimaryTextColor);
        mMonthTitlePaint.setTextAlign(Align.CENTER);
        mMonthTitlePaint.setStyle(Style.FILL);

        mSelectedCirclePaint = new Paint();
        mSelectedCirclePaint.setFakeBoldText(true);
        mSelectedCirclePaint.setAntiAlias(true);
        mSelectedCirclePaint.setColor(mController.getAccentColor());
        mSelectedCirclePaint.setTextAlign(Align.CENTER);
        mSelectedCirclePaint.setStyle(Style.FILL);
        mSelectedCirclePaint.setAlpha(SELECTED_CIRCLE_ALPHA);

        mMonthDayLabelPaint = new Paint();
        mMonthDayLabelPaint.setAntiAlias(true);
        mMonthDayLabelPaint.setFakeBoldText(true);
        mMonthDayLabelPaint.setTextSize(mMonthDayLabelTextSize);
        mMonthDayLabelPaint.setColor(mSecondaryTextColor);
        mMonthDayLabelPaint.setTypeface(boldFont);
        mMonthDayLabelPaint.setStyle(Style.FILL);
        mMonthDayLabelPaint.setTextAlign(Align.CENTER);

        mMonthNumPaint = new Paint();
        mMonthNumPaint.setAntiAlias(true);
        mMonthNumPaint.setTextSize(mMiniDayNumberTextSize);
        mMonthNumPaint.setStyle(Style.FILL);
        mMonthNumPaint.setTextAlign(Align.CENTER);
        mMonthNumPaint.setFakeBoldText(false);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawMonthTitle(canvas);
        drawMonthDayLabels(canvas);
        drawMonthNums(canvas);
    }

    public void setMonthParams(final int year,
                               final int month,
                               final int weekStart,
                               final int selectedDay) {
        mSelectedDay = selectedDay;

        // Allocate space for caching the day numbers and focus values
        mYear = year;
        mMonth = month;

        // Figure out what day today is
        //final Time today = new Time(Time.getCurrentTimezone());
        //today.setToNow();
        final Calendar today = CalendarCompat.getInstance();
        mHasToday = false;
        mToday = -1;

        mCalendar.set(Calendar.MONTH, mMonth);
        mCalendar.set(Calendar.YEAR, mYear);
        mCalendar.set(Calendar.DAY_OF_MONTH, 1);
        mDayOfWeekStart = mCalendar.get(Calendar.DAY_OF_WEEK);

        if (weekStart != CommonHelper.INVALID) {
            mWeekStart = weekStart;
        } else {
            mWeekStart = mCalendar.getFirstDayOfWeek();
        }

        mNumCells = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < mNumCells; i++) {
            final int day = i + 1;
            if (sameDay(day, today)) {
                mHasToday = true;
                mToday = day;
            }
        }
        mNumRows = calculateNumRows();
    }

    public void setSelectedDay(final int day) {
        mSelectedDay = day;
    }

    public void reuse() {
        mNumRows = DEFAULT_NUM_ROWS;
        requestLayout();
    }

    private int calculateNumRows() {
        int offset = findDayOffset();
        int dividend = (offset + mNumCells) / CommonHelper.DAYS_IN_WEEK;
        int remainder = (offset + mNumCells) % CommonHelper.DAYS_IN_WEEK;
        return (dividend + (remainder > 0 ? 1 : 0));
    }

    private boolean sameDay(final int day, final Calendar today) {
        return mYear == today.get(Calendar.YEAR) &&
                mMonth == today.get(Calendar.MONTH) &&
                day == today.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int bottomPadding = getContext().getResources().getDimensionPixelSize(R.dimen.mdtp_date_picker_bottom_padding);
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec),
                             mRowHeight * mNumRows + getMonthHeaderSize() + bottomPadding);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mWidth = w;
    }

    public int getMonth() {
        return mMonth;
    }

    public int getYear() {
        return mYear;
    }

    /**
     * A wrapper to the MonthHeaderSize to allow override it in children
     */
    protected int getMonthHeaderSize() {
        return mMonthHeaderSize;
    }

    @NonNull
    private String getMonthAndYearString() {
        return mFormatter.format(mCalendar.getTime());
    }

    protected void drawMonthTitle(final Canvas canvas) {
        final int x = (mWidth + 2 * mEdgePadding) / 2;
        final int y = (getMonthHeaderSize() - mMonthDayLabelTextSize) / 2;
        canvas.drawText(getMonthAndYearString(), x, y, mMonthTitlePaint);
    }

    protected void drawMonthDayLabels(final Canvas canvas) {
        final int y = getMonthHeaderSize() - (mMonthDayLabelTextSize / 2);
        final int dayWidthHalf = (mWidth - mEdgePadding * 2) / (CommonHelper.DAYS_IN_WEEK * 2);
        final int todayInWeekIndex = (mDayOfWeekStart - mWeekStart + mToday - 1) % CommonHelper.DAYS_IN_WEEK;
        boolean isToday;
        int dayInWeekIndex;
        int x;

        for (int i = 0; i < CommonHelper.DAYS_IN_WEEK; i++) {
            x = (2 * i + 1) * dayWidthHalf + mEdgePadding;
            if (mIsRtl) {
                x = mWidth - x;
            }
            dayInWeekIndex = DTPickersHelper.indexDayToShiftedWeekIndexDay(i, mWeekStart);
            isToday = mHasToday && todayInWeekIndex == i;

            if (isToday) {
                mMonthDayLabelPaint.setColor(mPrimaryTextColor);
            }
            canvas.drawText(getWeekDayLabel(dayInWeekIndex), x, y, mMonthDayLabelPaint);
            if (isToday) {
                mMonthDayLabelPaint.setColor(mSecondaryTextColor);
            }
        }
    }

    /**
     * Draws the week and month day numbers for this week. Override this method
     * if you need different placement.
     *
     * @param canvas The canvas to draw on
     */
    protected void drawMonthNums(Canvas canvas) {
        int y = (((mRowHeight + mMiniDayNumberTextSize) / 2) - mDaySeparatorWidth)
                + getMonthHeaderSize();

        final float dayWidthHalf = (mWidth - mEdgePadding * 2) / (CommonHelper.DAYS_IN_WEEK * 2.0f);
        int j = findDayOffset();
        int x;

        for (int dayNumber = 1; dayNumber <= mNumCells; dayNumber++) {
            x = (int) ((2 * j + 1) * dayWidthHalf + mEdgePadding);
            if (mIsRtl) {
                x = mWidth - x;
            }

            int yRelativeToDay = (mRowHeight + mMiniDayNumberTextSize) / 2 - mDaySeparatorWidth;

            final int startX = (int) (x - dayWidthHalf);
            final int stopX = (int) (x + dayWidthHalf);
            final int startY = (int) (y - yRelativeToDay);
            final int stopY = (int) (startY + mRowHeight);

            drawMonthDay(canvas, mYear, mMonth, dayNumber, x, y, startX, stopX, startY, stopY);

            j++;
            if (j == CommonHelper.DAYS_IN_WEEK) {
                j = 0;
                y += mRowHeight;
            }
        }
    }

    /**
     * This method should draw the month day.  Implemented by sub-classes to allow customization.
     *
     * @param canvas The canvas to draw on
     * @param year   The year of this month day
     * @param month  The month of this month day
     * @param day    The day number of this month day
     * @param x      The default x position to draw the day number
     * @param y      The default y position to draw the day number
     * @param startX The left boundary of the day number rect
     * @param stopX  The right boundary of the day number rect
     * @param startY The top boundary of the day number rect
     * @param stopY  The bottom boundary of the day number rect
     */
    public abstract void drawMonthDay(Canvas canvas, int year, int month, int day,
                                      int x, int y, int startX, int stopX, int startY, int stopY);

    protected int findDayOffset() {
        return (mDayOfWeekStart < mWeekStart ? (mDayOfWeekStart + CommonHelper.DAYS_IN_WEEK) : mDayOfWeekStart)
                - mWeekStart;
    }


    /**
     * Calculates the day that the given x position is in, accounting for week
     * number. Returns the day or -1 if the position wasn't in a day.
     *
     * @param x The x position of the touch event
     * @return The day number, or -1 if the position wasn't in a day
     */
    public int getDayFromLocation(float x, float y) {
        final int day = getInternalDayFromLocation(x, y);
        if (day < 1 || day > mNumCells) {
            return CommonHelper.INVALID;
        }
        return day;
    }

    /**
     * Calculates the day that the given x position is in, accounting for week
     * number.
     *
     * @param x The x position of the touch event
     * @return The day number
     */
    protected int getInternalDayFromLocation(float x, float y) {
        int dayStart = mEdgePadding;
        if (x < dayStart || x > mWidth - mEdgePadding) {
            return CommonHelper.INVALID;
        }
        // Selection is (x - start) / (pixels/day) == (x -s) * day / pixels
        int row = (int) (y - getMonthHeaderSize()) / mRowHeight;
        int column = (int) ((x - dayStart) * CommonHelper.DAYS_IN_WEEK / (mWidth - dayStart - mEdgePadding));
        if (mIsRtl) {
            column = CommonHelper.DAYS_IN_WEEK - 1 - column;
        }
        int day = column - findDayOffset() + 1;
        day += row * CommonHelper.DAYS_IN_WEEK;
        return day;
    }

    /**
     * Called when the user clicks on a day. Handles callbacks to the
     * {@link OnDayClickListener} if one is set.
     * <p/>
     * If the day is out of the range set by minDate and/or maxDate, this is a no-op.
     *
     * @param day The day that was clicked
     */
    private void onDayClick(int day) {
        // If the min / max date are set, only process the click if it's a valid selection.
        if (mController.isOutOfRange(mYear, mMonth, day)) {
            return;
        }

        if (mOnDayClickListener != null) {
            mOnDayClickListener.onDayClick(this, new MonthAdapter.CalendarDay(mYear, mMonth, day));
        }
    }

    /**
     * @param year
     * @param month
     * @param day
     * @return true if the given date should be highlighted
     */
    protected boolean isHighlighted(int year, int month, int day) {
        Calendar[] highlightedDays = mController.getHighlightedDays();
        if (highlightedDays == null) return false;
        for (Calendar c : highlightedDays) {
            if (year < c.get(Calendar.YEAR)) break;
            if (year > c.get(Calendar.YEAR)) continue;
            if (month < c.get(Calendar.MONTH)) break;
            if (month > c.get(Calendar.MONTH)) continue;
            if (day < c.get(Calendar.DAY_OF_MONTH)) break;
            if (day > c.get(Calendar.DAY_OF_MONTH)) continue;
            return true;
        }
        return false;
    }

    /**
     * Return a 1 or 2 letter String for use as a weekday label
     *
     * @return The weekday label
     */
    private String getWeekDayLabel(final int dayInWeekIndex) {
        return mController.getDaysOfWeek()[dayInWeekIndex + 1];
    }

    /**
     * Handles callbacks when the user clicks on a time object.
     */
    public interface OnDayClickListener {
        void onDayClick(MonthView view, MonthAdapter.CalendarDay day);
    }
}
