/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.datetimepickers.date;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.olekdia.commonhelpers.CommonHelper;

public class SimpleMonthView extends MonthView {

    public SimpleMonthView(Context context, AttributeSet attr, IDatePickerController controller) {
        super(context, attr, controller);
    }

    @Override
    public void drawMonthDay(Canvas canvas, int year, int month, int day,
                             int x, int y, int startX, int stopX, int startY, int stopY) {
        if (mSelectedDay == day) {
            canvas.drawCircle(x, y - (mMiniDayNumberTextSize / 3), mDaySelectedCircleSize,
                              mSelectedCirclePaint);
        }

        // If we have a mindate or maxdate, gray out the day number if it's outside the range.
        if (mController.isOutOfRange(year, month, day)) {
            mMonthNumPaint.setColor(mDisabledDayTextColor);
            mMonthNumPaint.setTypeface(mDefNormalTypeface);
            mMonthNumPaint.setFakeBoldText(false);
        } else if (mSelectedDay == day) {
            mMonthNumPaint.setTypeface(mDefBoldTypeface);
            mMonthNumPaint.setColor(mSelectedDayTextColor);
            mMonthNumPaint.setFakeBoldText(true);
        } else if (mHasToday && mToday == day) {
            mMonthNumPaint.setColor(mPrimaryTextColor);
            mMonthNumPaint.setFakeBoldText(true);
            mMonthNumPaint.setTypeface(mDefBoldTypeface);
        } else {
            final boolean isHighlighted = isHighlighted(year, month, day);
            mMonthNumPaint.setColor(isHighlighted ? mHighlightedDayTextColor : mSecondaryTextColor);
            mMonthNumPaint.setTypeface(isHighlighted ? mDefBoldTypeface : mDefNormalTypeface);
            mMonthNumPaint.setFakeBoldText(false);
        }

        canvas.drawText(CommonHelper.formatNumber31(day, mController.getNumeralSystem()), x, y, mMonthNumPaint);
    }
}
