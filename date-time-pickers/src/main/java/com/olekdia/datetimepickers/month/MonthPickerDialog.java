/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.datetimepickers.month;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.olekdia.commonhelpers.CommonHelper;
import com.olekdia.commonhelpers.MathHelper;
import com.olekdia.datetimepickers.DTPickersHelper;
import com.olekdia.datetimepickers.HapticFeedbackController;
import com.olekdia.datetimepickers.ICommonDatePickerController;
import com.olekdia.datetimepickers.IOnDateChangedListener;
import com.olekdia.datetimepickers.PickerDialog;
import com.olekdia.datetimepickers.R;
import com.olekdia.datetimepickers.calendars.CalendarCompat;
import com.olekdia.datetimepickers.date.DateAnimator;
import com.olekdia.datetimepickers.year.YearsPickerView;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

/**
 * Dialog allowing users to select a month.
 */
public class MonthPickerDialog extends PickerDialog implements
        OnClickListener, IMonthPickerController {

    private static final int MONTH_VIEW = 0;

    private final Calendar mCalendar = CalendarCompat.getInstance();
    private OnDateSetListener mCallBack;
    private HashSet<IOnDateChangedListener> mListeners = new HashSet<>();

    private DateAnimator mAnimator;
    private boolean mDelayAnimation = true;

    private LinearLayout mMonthAndYearView;
    private TextView mSelectedMonthTextView;
    private TextView mYearView;
    private YearPickerView mYearPickerView;
    private YearsPickerView mYearsPickerView;

    private int mCurrentView = CommonHelper.INVALID;
    private int mDefaultView = MONTH_VIEW;

    private int mMinYear = MathHelper.roundTo(mCalendar.get(Calendar.YEAR) - 100, 100);
    private int mMaxYear = MathHelper.roundTo(mCalendar.get(Calendar.YEAR) + 200, 100);
    private Calendar mMinDate;
    private Calendar mMaxDate;
    private Calendar[] mHighlightedMonths;
    private Calendar[] mSelectableMonths;

    private final SimpleDateFormat mFormatter = new SimpleDateFormat("LLLL", Locale.getDefault());
    private final Date mTempDate = new Date();

    /**
     * The callback used to indicate the user is done filling in the date.
     */
    public interface OnDateSetListener {

        /**
         * @param view        The view associated with this listener.
         * @param year        The year that was set.
         * @param monthOfYear The month that was set (0-11) for compatibility
         *                    with {@link Calendar}.
         */
        void onDateSet(MonthPickerDialog view, int year, int monthOfYear);
    }

    public MonthPickerDialog() {
        // Empty constructor required for dialog fragment.
    }

    public void initialize(OnDateSetListener callBack, int year, int monthOfYear) {
        mCallBack = callBack;
        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, monthOfYear);
    }

//--------------------------------------------------------------------------------------------------
//  Lifecycle
//--------------------------------------------------------------------------------------------------

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Activity activity = getActivity();
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mCurrentView = CommonHelper.INVALID;
        if (savedInstanceState != null) {
            mCalendar.set(Calendar.YEAR, savedInstanceState.getInt(KEY_SELECTED_YEAR));
            mCalendar.set(Calendar.MONTH, savedInstanceState.getInt(KEY_SELECTED_MONTH));
            mCalendar.set(Calendar.DAY_OF_MONTH, savedInstanceState.getInt(KEY_SELECTED_DAY));
            mDefaultView = savedInstanceState.getInt(KEY_DEFAULT_VIEW);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_SELECTED_YEAR, mCalendar.get(Calendar.YEAR));
        outState.putInt(KEY_SELECTED_MONTH, mCalendar.get(Calendar.MONTH));
        outState.putInt(KEY_SELECTED_DAY, mCalendar.get(Calendar.DAY_OF_MONTH));
        outState.putInt(KEY_YEAR_START, mMinYear);
        outState.putInt(KEY_YEAR_END, mMaxYear);
        outState.putInt(KEY_CURRENT_VIEW, mCurrentView);
        int listPosition = CommonHelper.INVALID;
        if (mCurrentView == MONTH_VIEW) {
            listPosition = mYearPickerView.getMostVisiblePosition();
        } else if (mCurrentView == ICommonDatePickerController.YEAR_VIEW) {
            listPosition = mYearsPickerView.getFirstVisiblePosition();
            outState.putInt(KEY_LIST_POSITION_OFFSET, mYearsPickerView.getFirstPositionOffset());
        }
        outState.putInt(KEY_LIST_POSITION, listPosition);
        outState.putSerializable(KEY_MIN_DATE, mMinDate);
        outState.putSerializable(KEY_MAX_DATE, mMaxDate);
        outState.putSerializable(KEY_HIGHLIGHTED_DAYS, mHighlightedMonths);
        outState.putSerializable(KEY_SELECTABLE_DAYS, mSelectableMonths);
        outState.putInt(KEY_DEFAULT_VIEW, mDefaultView);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // All options have been set at this point: round the initial selection if necessary
        setToNearestDate(mCalendar);

        View view = inflater.inflate(R.layout.mdtp_month_picker_dialog, container, false);

        mMonthAndYearView = (LinearLayout) view.findViewById(R.id.month_picker_month_and_year);
        mMonthAndYearView.setOnClickListener(this);
        mSelectedMonthTextView = (TextView) view.findViewById(R.id.month_picker_month);
        mYearView = (TextView) view.findViewById(R.id.month_picker_year);
        mYearView.setOnClickListener(this);

        int listPosition = CommonHelper.INVALID;
        int listPositionOffset = 0;
        int currentView = mDefaultView;
        if (savedInstanceState != null) {
            mMinYear = savedInstanceState.getInt(KEY_YEAR_START);
            mMaxYear = savedInstanceState.getInt(KEY_YEAR_END);
            currentView = savedInstanceState.getInt(KEY_CURRENT_VIEW);
            listPosition = savedInstanceState.getInt(KEY_LIST_POSITION);
            listPositionOffset = savedInstanceState.getInt(KEY_LIST_POSITION_OFFSET);
            mMinDate = (Calendar) savedInstanceState.getSerializable(KEY_MIN_DATE);
            mMaxDate = (Calendar) savedInstanceState.getSerializable(KEY_MAX_DATE);
            mHighlightedMonths = (Calendar[]) savedInstanceState.getSerializable(KEY_HIGHLIGHTED_DAYS);
            mSelectableMonths = (Calendar[]) savedInstanceState.getSerializable(KEY_SELECTABLE_DAYS);
            mDefaultView = savedInstanceState.getInt(KEY_DEFAULT_VIEW);
            restoreInstanceState(savedInstanceState);
        }

        final Activity activity = getActivity();
        mYearPickerView = new YearPickerView(activity, this);
        mYearsPickerView = new YearsPickerView(activity, this);

        // if theme mode has not been set by java code, check if it is specified in Style.xml
        if (!mThemeDarkChanged) {
            mThemeDark = DTPickersHelper.isDarkTheme(activity, mThemeDark);
        }

        final int bgColorResource = mThemeDark ? R.color.mdtp_date_picker_view_animator_dark_theme
                : R.color.mdtp_date_picker_view_animator;
        view.setBackgroundColor(ContextCompat.getColor(activity, bgColorResource));

        mAnimator = (DateAnimator) view.findViewById(R.id.animator);
        mAnimator.addView(mYearPickerView);
        mAnimator.addView(mYearsPickerView);
        final Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(ICommonDatePickerController.ANIMATION_DURATION);
        mAnimator.setInAnimation(animation);
        final Animation animation2 = new AlphaAnimation(1.0f, 0.0f);
        animation2.setDuration(ICommonDatePickerController.ANIMATION_DURATION);
        mAnimator.setOutAnimation(animation2);

        final Button okButton = (Button) view.findViewById(R.id.ok);
        okButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                tryVibrate();
                notifyOnDateListener();
                dismiss();
            }
        });
        okButton.setTypeface(CommonHelper.getTypeface(activity, CommonHelper.ROBOTO_MEDIUM_FONT));
        if (mOkString != null) {
            okButton.setText(mOkString);
        } else {
            okButton.setText(mOkResId);
        }

        final Button cancelButton = (Button) view.findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tryVibrate();
                if (getDialog() != null) getDialog().cancel();
            }
        });
        cancelButton.setTypeface(CommonHelper.getTypeface(activity, CommonHelper.ROBOTO_MEDIUM_FONT));
        if (mCancelString != null) {
            cancelButton.setText(mCancelString);
        } else {
            cancelButton.setText(mCancelResId);
        }
        cancelButton.setVisibility(isCancelable() ? View.VISIBLE : View.GONE);

        // If an accent color has not been set manually, get it from the context
        if (mAccentColor == CommonHelper.INVALID) {
            mAccentColor = DTPickersHelper.getAccentColorFromThemeIfAvailable(activity);
        }
        if (mPrimaryColor == CommonHelper.INVALID) {
            mPrimaryColor = DTPickersHelper.getPrimaryColorFromThemeIfAvailable(activity);
        }
        okButton.setTextColor(mAccentColor);
        cancelButton.setTextColor(mAccentColor);
        view.findViewById(R.id.month_picker_selected_month_layout).setBackgroundColor(mPrimaryColor);

        if (getDialog() == null) {
            view.findViewById(R.id.done_background).setVisibility(View.GONE);
        }

        updateDisplay();
        setCurrentView(currentView);

        if (listPosition != CommonHelper.INVALID) {
            if (currentView == MONTH_VIEW) {
                mYearPickerView.postSetSelection(listPosition);
            } else if (currentView == ICommonDatePickerController.YEAR_VIEW) {
                mYearsPickerView.postSetSelectionFromTop(listPosition, listPositionOffset);
            }
        }

        mHapticFeedbackController = new HapticFeedbackController(activity);
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

//--------------------------------------------------------------------------------------------------
//  Methods
//--------------------------------------------------------------------------------------------------

    private void setCurrentView(final int viewIndex) {
        switch (viewIndex) {
            case MONTH_VIEW:
                ObjectAnimator pulseAnimator = DTPickersHelper.getPulseAnimator(mMonthAndYearView, 0.9f, 1.05f);
                if (mDelayAnimation) {
                    pulseAnimator.setStartDelay(ICommonDatePickerController.ANIMATION_DELAY);
                    mDelayAnimation = false;
                }
                mYearPickerView.onDateChanged();
                if (mCurrentView != viewIndex) {
                    mMonthAndYearView.setSelected(true);
                    mYearView.setSelected(false);
                    mAnimator.setDisplayedChild(MONTH_VIEW);
                    mCurrentView = viewIndex;
                }
                pulseAnimator.start();
                break;
            case ICommonDatePickerController.YEAR_VIEW:
                pulseAnimator = DTPickersHelper.getPulseAnimator(mYearView, 0.85f, 1.1f);
                if (mDelayAnimation) {
                    pulseAnimator.setStartDelay(ICommonDatePickerController.ANIMATION_DELAY);
                    mDelayAnimation = false;
                }
                mYearsPickerView.onDateChanged();
                if (mCurrentView != viewIndex) {
                    mMonthAndYearView.setSelected(false);
                    mYearView.setSelected(true);
                    mAnimator.setDisplayedChild(ICommonDatePickerController.YEAR_VIEW);
                    mCurrentView = viewIndex;
                }
                pulseAnimator.start();
                break;
        }
    }

    private void updateDisplay() {
        mTempDate.setMonth(mCalendar.get(Calendar.MONTH));
        final String monthTitle = mFormatter.format(mTempDate);

        mSelectedMonthTextView.setText(monthTitle);
        mYearView.setText(CommonHelper.formatInt(mCalendar.get(Calendar.YEAR), mNumeralSystem));
    }

    private void updatePickers() {
        for (IOnDateChangedListener listener : mListeners) listener.onDateChanged();
    }

//--------------------------------------------------------------------------------------------------
//  Getters & setters
//--------------------------------------------------------------------------------------------------

    @SuppressWarnings("unused")
    public void setYearRange(final int startYear, final int endYear) {
        if (endYear < startYear) {
            throw new IllegalArgumentException("Year end must be larger than or equal to year start");
        }

        mMinYear = startYear;
        mMaxYear = endYear;
        if (mYearPickerView != null) {
            mYearPickerView.onChange();
        }
    }

    /**
     * Sets the minimal date supported by this DatePicker. Dates before (but not including) the
     * specified date will be disallowed from being selected.
     *
     * @param calendar a Calendar object set to the year, month, day desired as the mindate.
     */
    @SuppressWarnings("unused")
    public void setMinDate(final Calendar calendar) {
        mMinDate = calendar;

        if (mYearPickerView != null) {
            mYearPickerView.onChange();
        }
    }

    /**
     * @return The minimal date supported by this DatePicker. Null if it has not been set.
     */
    @SuppressWarnings("unused")
    public Calendar getMinDate() {
        return mMinDate;
    }

    /**
     * Sets the minimal date supported by this DatePicker. Dates after (but not including) the
     * specified date will be disallowed from being selected.
     *
     * @param calendar a Calendar object set to the year, month, day desired as the maxdate.
     */
    @SuppressWarnings("unused")
    public void setMaxDate(final Calendar calendar) {
        mMaxDate = calendar;

        if (mYearPickerView != null) {
            mYearPickerView.onChange();
        }
    }

    /**
     * @return The maximal date supported by this DatePicker. Null if it has not been set.
     */
    @SuppressWarnings("unused")
    public Calendar getMaxDate() {
        return mMaxDate;
    }

    /**
     * Sets an array of dates which should be highlighted when the picker is drawn
     *
     * @param highlightedMonths an Array of Calendar objects containing the dates to be highlighted
     */
    @SuppressWarnings("unused")
    public void setHighlightedDays(final Calendar[] highlightedMonths) {
        // Sort the array to optimize searching over it later on
        Arrays.sort(highlightedMonths);
        mHighlightedMonths = highlightedMonths;
    }

    /**
     * @return The list of dates, as Calendar Objects, which should be highlighted. null is no dates should be highlighted
     */
    @Override
    public Calendar[] getHighlightedMonths() {
        return mHighlightedMonths;
    }

    /**
     * Set's a list of days which are the only valid selections.
     * Setting this value will take precedence over using setMinDate() and setMaxDate()
     *
     * @param selectableMonths an Array of Calendar Objects containing the selectable dates
     */
    @SuppressWarnings("unused")
    public void setSelectableMonths(final Calendar[] selectableMonths) {
        // Sort the array to optimize searching over it later on
        Arrays.sort(selectableMonths);
        mSelectableMonths = selectableMonths;
    }

    /**
     * @return an Array of Calendar objects containing the list with selectable items. null if no restriction is set
     */
    @Override
    public Calendar[] getSelectableMonths() {
        return mSelectableMonths;
    }

    @SuppressWarnings("unused")
    public void setOnDateSetListener(final OnDateSetListener listener) {
        mCallBack = listener;
    }

    @Override
    public YearAdapter.CalendarMonth getSelectedMonth() {
        return new YearAdapter.CalendarMonth(mCalendar);
    }

    @Override
    public int getSelectedYear() {
        return mCalendar.get(Calendar.YEAR);
    }

    @Override
    public Calendar getStartDate() {
        if (mSelectableMonths != null) return mSelectableMonths[0];
        if (mMinDate != null) return mMinDate;
        final Calendar output = Calendar.getInstance();
        output.set(Calendar.YEAR, mMinYear);
        output.set(Calendar.DAY_OF_MONTH, 1);
        output.set(Calendar.MONTH, Calendar.JANUARY);
        return output;
    }

    @Override
    public Calendar getEndDate() {
        if (mSelectableMonths != null) return mSelectableMonths[mSelectableMonths.length - 1];
        if (mMaxDate != null) return mMaxDate;
        final Calendar output = Calendar.getInstance();
        output.set(Calendar.YEAR, mMaxYear);
        output.set(Calendar.DAY_OF_MONTH, 31);
        output.set(Calendar.MONTH, Calendar.DECEMBER);
        return output;
    }

    @Override
    public int getMinYear() {
        if (mSelectableMonths != null) return mSelectableMonths[0].get(Calendar.YEAR);
        // Ensure no years can be selected outside of the given minimum date
        return mMinDate != null && mMinDate.get(Calendar.YEAR) > mMinYear ? mMinDate.get(Calendar.YEAR) : mMinYear;
    }

    @Override
    public int getMaxYear() {
        if (mSelectableMonths != null)
            return mSelectableMonths[mSelectableMonths.length - 1].get(Calendar.YEAR);
        // Ensure no years can be selected outside of the given maximum date
        return mMaxDate != null && mMaxDate.get(Calendar.YEAR) < mMaxYear ? mMaxDate.get(Calendar.YEAR) : mMaxYear;
    }

    /**
     * @return true if the specified year/month/day are within the selectable days or the range set by minDate and maxDate.
     * If one or either have not been set, they are considered as Integer.MIN_VALUE and
     * Integer.MAX_VALUE.
     */
    @Override
    public boolean isOutOfRange(final int year, final int month) {
        if (mSelectableMonths != null) {
            return !isSelectable(year, month);
        }

        if (isBeforeMin(year, month)) {
            return true;
        } else if (isAfterMax(year, month)) {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unused")
    public boolean isOutOfRange(final Calendar calendar) {
        return isOutOfRange(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));
    }

    private boolean isSelectable(final int year, final int month) {
        for (Calendar c : mSelectableMonths) {
            if (year < c.get(Calendar.YEAR)) break;
            if (year > c.get(Calendar.YEAR)) continue;
            if (month < c.get(Calendar.MONTH)) break;
            if (month > c.get(Calendar.MONTH)) continue;
            return true;
        }
        return false;
    }

    private boolean isBeforeMin(final int year, final int month) {
        if (mMinDate == null) {
            return false;
        }

        if (year < mMinDate.get(Calendar.YEAR)) {
            return true;
        } else if (year > mMinDate.get(Calendar.YEAR)) {
            return false;
        }

        if (month < mMinDate.get(Calendar.MONTH)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isBeforeMin(final Calendar calendar) {
        return isBeforeMin(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));
    }

    private boolean isAfterMax(final int year, final int month) {
        if (mMaxDate == null) {
            return false;
        }

        if (year > mMaxDate.get(Calendar.YEAR)) {
            return true;
        } else if (year < mMaxDate.get(Calendar.YEAR)) {
            return false;
        }

        if (month > mMaxDate.get(Calendar.MONTH)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isAfterMax(final Calendar calendar) {
        return isAfterMax(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));
    }

    private void setToNearestDate(final Calendar calendar) {
        if (mSelectableMonths != null) {
            long distance = Long.MAX_VALUE;
            Calendar currentBest = calendar;
            for (Calendar c : mSelectableMonths) {
                long newDistance = Math.abs(calendar.getTimeInMillis() - c.getTimeInMillis());
                if (newDistance < distance) {
                    distance = newDistance;
                    currentBest = c;
                } else break;
            }
            calendar.setTimeInMillis(currentBest.getTimeInMillis());
            return;
        }

        if (isBeforeMin(calendar)) {
            calendar.setTimeInMillis(mMinDate.getTimeInMillis());
            return;
        }

        if (isAfterMax(calendar)) {
            calendar.setTimeInMillis(mMaxDate.getTimeInMillis());
            return;
        }
    }

    @Override
    public void registerOnDateChangedListener(final IOnDateChangedListener listener) {
        mListeners.add(listener);
    }

    @Override
    public void unregisterOnDateChangedListener(final IOnDateChangedListener listener) {
        mListeners.remove(listener);
    }

//--------------------------------------------------------------------------------------------------
//  Event handlers
//--------------------------------------------------------------------------------------------------

    public void notifyOnDateListener() {
        if (mCallBack != null) {
            mCallBack.onDateSet(MonthPickerDialog.this,
                                mCalendar.get(Calendar.YEAR),
                                mCalendar.get(Calendar.MONTH));
        }
    }

    @Override
    public void onClick(View v) {
        tryVibrate();
        if (v.getId() == R.id.month_picker_year) {
            setCurrentView(ICommonDatePickerController.YEAR_VIEW);
        } else if (v.getId() == R.id.month_picker_month_and_year) {
            setCurrentView(MONTH_VIEW);
        }
    }

    @Override
    public void onYearSelected(final int year) {
        mCalendar.set(Calendar.YEAR, year);
        updatePickers();
        setCurrentView(MONTH_VIEW);
        updateDisplay();
    }

    @Override
    public void onMonthSelected(final int year, final int month) {
        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, month);
        mCalendar.set(Calendar.DAY_OF_MONTH, 1);
        updatePickers();
        updateDisplay();
        if (mAutoDismiss) {
            notifyOnDateListener();
            dismiss();
        }
    }
}