/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.datetimepickers.month;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.util.AttributeSet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SimpleYearView extends YearView {

    private final SimpleDateFormat mFormat;
    private final Date mTempDate;
    private final RectF mSelectedRect;

    public SimpleYearView(Context context, AttributeSet attr, IMonthPickerController controller) {
        super(context, attr, controller);

        mFormat = new SimpleDateFormat("LLLL", Locale.getDefault());
        mTempDate = new Date();
        mSelectedRect = new RectF();
    }

    @Override
    public void drawYearMonth(Canvas canvas, int year, int month, float x, float y) {
        final boolean isSelected = mSelectedMonth == month && mSelectedYear == year;
        final boolean isCurrent = mCurrantMonth == month && mCurrantYear == year;

        final String monthTitle = getMonthTitle(month);

        if (isSelected) {
            final float rectWidth = mMonthTitlePaint.measureText(monthTitle, 0, monthTitle.length());
            mSelectedRect.set(x - rectWidth / 2 - mSelectedRectHeight,
                              y - 2 * mSelectedRectHeight,
                              x - rectWidth / 2 + rectWidth + mSelectedRectHeight,
                              y + mSelectedRectHeight);
            canvas.drawRoundRect(mSelectedRect, mSelectedRectHeight, mSelectedRectHeight, mSelectedRectPaint);
        }

        if (isSelected) {
            mMonthTitlePaint.setColor(mSelectedMonthTextColor);
            mMonthTitlePaint.setTypeface(mDefBoldTypeface);
        } else if (isCurrent) {
            mMonthTitlePaint.setColor(mPrimaryTextColor);
            mMonthTitlePaint.setTypeface(mDefBoldTypeface);
        } else {
            mMonthTitlePaint.setColor(mSecondaryTextColor);
            mMonthTitlePaint.setTypeface(mDefNormalTypeface);
        }
        mMonthTitlePaint.setFakeBoldText(isSelected | isCurrent);

        canvas.drawText(monthTitle, x, y, mMonthTitlePaint);
    }

    public String getMonthTitle(final int month) {
        mTempDate.setMonth(month);
        return mFormat.format(mTempDate);
    }
}
