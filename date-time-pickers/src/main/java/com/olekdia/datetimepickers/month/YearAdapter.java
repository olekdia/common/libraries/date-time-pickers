/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.datetimepickers.month;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;

import com.olekdia.datetimepickers.calendars.CalendarCompat;

import java.util.Calendar;

/**
 * An adapter for a list of {@link YearView} items.
 */
public class YearAdapter extends BaseAdapter implements YearView.OnMonthClickListener {

    private final Context mContext;
    protected final IMonthPickerController mController;

    private CalendarMonth mSelectedMonth;

    /**
     * A convenience class to represent a specific date.
     */
    public static class CalendarMonth {
        private Calendar calendar;
        int year;
        int month;

        public CalendarMonth() {
            setTime(System.currentTimeMillis());
        }

        public CalendarMonth(long timeInMillis) {
            setTime(timeInMillis);
        }

        public CalendarMonth(Calendar calendar) {
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
        }

        public CalendarMonth(int year, int month) {
            setMonth(year, month);
        }

        public void set(final CalendarMonth date) {
            year = date.year;
            month = date.month;
        }

        public void setMonth(final int year, final int month) {
            this.year = year;
            this.month = month;
        }

        private void setTime(final long timeInMillis) {
            if (calendar == null) {
                calendar = CalendarCompat.getInstance();
            }
            calendar.setTimeInMillis(timeInMillis);
            month = calendar.get(Calendar.MONTH);
            year = calendar.get(Calendar.YEAR);
        }

        public int getYear() {
            return year;
        }

        public int getMonth() {
            return month;
        }
    }

    public YearAdapter(Context context, IMonthPickerController controller) {
        mContext = context;
        mController = controller;
        init();
        setSelectedDay(mController.getSelectedMonth());
    }

    /**
     * Updates the selected day and related parameters.
     *
     * @param day The day to highlight
     */
    public void setSelectedDay(final CalendarMonth day) {
        mSelectedMonth = day;
        notifyDataSetChanged();
    }

    @SuppressWarnings("unused")
    public CalendarMonth getSelectedDay() {
        return mSelectedMonth;
    }

    /**
     * Set up the gesture detector and selected time
     */
    protected void init() {
        mSelectedMonth = new CalendarMonth(System.currentTimeMillis());
    }

    @Override
    public int getCount() {
        final Calendar endDate = mController.getEndDate();
        final Calendar startDate = mController.getStartDate();
        return endDate.get(Calendar.YEAR) - startDate.get(Calendar.YEAR) + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("unchecked")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        YearView v;

        if (convertView != null) {
            v = (YearView) convertView;
        } else {
            v = createMonthView(mContext);
            // Set up the new view
            LayoutParams params = new LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            v.setLayoutParams(params);
            v.setClickable(true);
            v.setOnDayClickListener(this);
        }

        final int year = position + mController.getStartDate().get(Calendar.MONTH) + mController.getMinYear();

        // Invokes requestLayout() to ensure that the recycled view is set with the appropriate
        // height/number of weeks before being displayed.
        v.reuse();

        v.setMonthParams(year);
        if(mSelectedMonth != null){
            v.setSelectedYear(mSelectedMonth.year);
            v.setSelectedMonth(mSelectedMonth.month);
        }
        v.invalidate();
        return v;
    }

    public YearView createMonthView(Context context) {
        return new SimpleYearView(context, null, mController);
    }

    @Override
    public void onMonthClick(YearView view, CalendarMonth month) {
        if (month != null) {
            mController.onMonthSelected(month.year, month.month);

            mSelectedMonth.setMonth(month.year, month.month);
        }
    }
}
