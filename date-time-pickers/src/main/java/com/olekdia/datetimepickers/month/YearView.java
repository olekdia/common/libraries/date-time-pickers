/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.datetimepickers.month;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.olekdia.commonhelpers.CommonHelper;
import com.olekdia.datetimepickers.R;
import com.olekdia.datetimepickers.calendars.CalendarCompat;
import com.olekdia.datetimepickers.month.YearAdapter.CalendarMonth;

import java.util.Calendar;

public abstract class YearView extends View {

    public static final int NUM_ROWS = 4;
    public static final int NUM_COLUMNS = 3;

    public static final int FIRST_MONTH = Calendar.JANUARY;

    private final Calendar mCalendarForDraw;
    private final Calendar mCalendar;

    private final boolean mIsRtl;

    protected final int mMonthTitleTextSize;
    protected final int mYearTextSize;
    protected final int mYearHeaderSize;
    protected float mSelectedRectHeight;
    private float mYearTopPadding;

    private String mYearTitleTypeface;
    protected Typeface mDefNormalTypeface;
    protected Typeface mDefBoldTypeface;

    protected Paint mMonthTitlePaint;
    protected Paint mYearTitlePaint;
    protected Paint mSelectedRectPaint;

    protected int mPrimaryTextColor;
    protected int mSecondaryTextColor;
    protected int mSelectedMonthTextColor;

    protected final IMonthPickerController mController;

    // Quick reference to the width of this view, matches parent
    protected int mWidth;
    protected final int mRowHeight;

    // Optional listener for handling day click actions
    protected OnMonthClickListener mOnMonthClickListener;

    protected int mCurrantMonth;
    protected int mCurrantYear;

    protected int mSelectedMonth;
    protected int mSelectedYear;

    protected int mYear;

    //--------------------------------------------------------------------------------------------------
//Constructors
//--------------------------------------------------------------------------------------------------
    public YearView(Context context) {
        this(context, null, null);
    }

    public YearView(Context context, AttributeSet attr, IMonthPickerController controller) {
        super(context, attr);

        mSelectedMonth = -1;
        mSelectedYear = -1;

        mController = controller;
        final Resources res = context.getResources();

        mCalendarForDraw = CalendarCompat.getInstance();
        mCalendar = CalendarCompat.getInstance();

        mIsRtl = CommonHelper.isLayoutRtl(res);

        mCurrantMonth = mCalendar.get(Calendar.MONTH);
        mCurrantYear = mCalendar.get(Calendar.YEAR);

        mYearTitleTypeface = res.getString(R.string.mdtp_sans_serif);

        mDefNormalTypeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL);
        mDefBoldTypeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD);

        final boolean darkTheme = mController != null && mController.isThemeDark();
        if (darkTheme) {
            mPrimaryTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_normal_dark_theme);
            mSecondaryTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_month_day_dark_theme);
        } else {
            mPrimaryTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_normal);
            mSecondaryTextColor = ContextCompat.getColor(context, R.color.mdtp_date_picker_month_day);
        }
        mSelectedMonthTextColor = ContextCompat.getColor(context, R.color.mdtp_white);

        mMonthTitleTextSize = res.getDimensionPixelSize(R.dimen.mdtp_day_number_size);
        mYearTextSize = res.getDimensionPixelSize(R.dimen.mdtp_month_label_size);
        mYearHeaderSize = res.getDimensionPixelOffset(R.dimen.mdtp_year_list_item_header_height);
        mYearTopPadding = mYearHeaderSize / 2 + mYearTextSize * 0.4F;

        mRowHeight = (res.getDimensionPixelOffset(R.dimen.mdtp_date_picker_view_animator_height)
                - getMonthHeaderSize() * 2) / NUM_ROWS;

        // Sets up any standard paints that will be used
        initView();
    }

    /**
     * Sets up the text and style properties for painting. Override this if you
     * want to use a different paint.
     */
    protected void initView() {
        mYearTitlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mYearTitlePaint.setFakeBoldText(true);
        mYearTitlePaint.setTextSize(mYearTextSize);
        mYearTitlePaint.setTypeface(Typeface.create(mYearTitleTypeface, Typeface.BOLD));
        mYearTitlePaint.setColor(mPrimaryTextColor);
        mYearTitlePaint.setTextAlign(Align.CENTER);
        mYearTitlePaint.setStyle(Style.FILL);

        mMonthTitlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mMonthTitlePaint.setTextSize(mMonthTitleTextSize);
        mMonthTitlePaint.setStyle(Style.FILL);
        mMonthTitlePaint.setTextAlign(Align.CENTER);
        mMonthTitlePaint.setFakeBoldText(true);
        final Rect bounds = new Rect();
        mMonthTitlePaint.getTextBounds("0", 0, 1, bounds);
        mSelectedRectHeight = bounds.height();

        mSelectedRectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mSelectedRectPaint.setColor(mController.getAccentColor());
        mSelectedRectPaint.setStyle(Style.FILL);
    }

//--------------------------------------------------------------------------------------------------

    public void setSelectedMonth(final int selectedMonth) {
        mSelectedMonth = selectedMonth;
    }

    public void setSelectedYear(int selectedYear) {
        mSelectedYear = selectedYear;
    }

    public void setOnDayClickListener(final OnMonthClickListener listener) {
        mOnMonthClickListener = listener;
    }

    public void setMonthParams(final int year) {
        mYear = year;

        mCalendar.set(Calendar.YEAR, mYear);
        mCalendar.set(Calendar.MONTH, FIRST_MONTH);
        mCalendar.set(Calendar.DAY_OF_MONTH, 1);
    }

    public int getYear() {
        return mYear;
    }

    public void reuse() {
        requestLayout();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mWidth = w;
    }

//--------------------------------------------------------------------------------------------------
//Draw methods
//--------------------------------------------------------------------------------------------------
    @Override
    protected void onDraw(Canvas canvas) {
        drawYearTitle(canvas);
        drawYearMonths(canvas);
    }

    protected void drawYearTitle(final Canvas canvas) {
        final int year = mCalendar.get(Calendar.YEAR);
        canvas.drawText(getYearString(year), mWidth / 2, mYearTopPadding, mYearTitlePaint);
    }

    /**
     * A wrapper to the MonthHeaderSize to allow override it in children
     */
    protected int getMonthHeaderSize() {
        return mYearHeaderSize;
    }

    //return current year(String, format: yyyy)
    @NonNull
    private String getYearString(final int year) {
        return CommonHelper.formatInt(year, mController.getNumeralSystem());
    }

    protected void drawYearMonths(final Canvas canvas) {
        final float columnW = mWidth / NUM_COLUMNS;
        final float rowH = mRowHeight;
        final float firstColumnX = mIsRtl ? mWidth - columnW / 2 : columnW / 2;
        final float nextColumnDelta = mIsRtl ? -columnW : columnW;
        float x = firstColumnX;
        float y = getMonthHeaderSize() + rowH / 2;

        for (int i = 1; i <= NUM_COLUMNS * NUM_ROWS; i++) {
            mCalendarForDraw.set(Calendar.MONTH, FIRST_MONTH - 1 + i);
            mCalendarForDraw.set(Calendar.YEAR, mYear);
            drawYearMonth(canvas, mCalendarForDraw.get(Calendar.YEAR), mCalendarForDraw.get(Calendar.MONTH), x, y);
            x += nextColumnDelta;
            if (i % NUM_COLUMNS == 0) {
                x = firstColumnX;
                y += rowH;
            }
        }
    }

    public abstract void drawYearMonth(Canvas canvas, int year, int month, float x, float y);

//--------------------------------------------------------------------------------------------------
// Handler for event
//--------------------------------------------------------------------------------------------------

    /**
     * Handles callbacks when the user clicks on a time object.
     */
    public interface OnMonthClickListener {
        void onMonthClick(YearView view, CalendarMonth month);
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                final int month = getMonthFromLocation(event.getX(), event.getY());
                if (month >= 0) {
                    onMonthClick(month);
                }
                break;
        }
        return true;
    }

    public int getMonthFromLocation(final float x, final float y) {
        final float columnW = mWidth / NUM_COLUMNS;
        final float rowH = mRowHeight;
        final int cellX = (int) Math.floor(mIsRtl ? (mWidth - x) / columnW : x / columnW);
        final int cellY = (int) Math.floor((y - getMonthHeaderSize()) / rowH);
        return cellY * NUM_COLUMNS + cellX;
    }

    /**
     * Called when the user clicks on a month. Handles callbacks to the
     * {@link OnMonthClickListener} if one is set.
     * <p/>
     *
     * @param month The month that was clicked
     */
    private void onMonthClick(final int month) {
        if (mController.isOutOfRange(mYear, month)) {
            return;
        }

        if (mOnMonthClickListener != null) {
            mOnMonthClickListener.onMonthClick(this, new CalendarMonth(mYear, month));
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec),
                             getMonthHeaderSize() + mRowHeight * NUM_ROWS);
    }

//--------------------------------------------------------------------------------------------------
}