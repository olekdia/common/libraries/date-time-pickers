/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.olekdia.datetimepickers.time;

import android.animation.ObjectAnimator;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.olekdia.commonhelpers.CommonHelper;
import com.olekdia.datetimepickers.DTPickersHelper;
import com.olekdia.datetimepickers.HapticFeedbackController;
import com.olekdia.datetimepickers.PickerDialog;
import com.olekdia.datetimepickers.R;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Dialog to set a time.
 */
public class TimePickerDialog extends PickerDialog implements
        RadialPickerLayout.OnValueSelectedListener, ITimePickerController {

    public static final int HOUR_INDEX = 0;
    public static final int MINUTE_INDEX = 1;
    public static final int SECOND_INDEX = 2;

    // Delay before starting the pulse animation, in ms
    private static final int PULSE_ANIMATOR_DELAY = 300;

    private OnTimeSetListener mCallback;

    private Button mCancelButton;
    private Button mOkButton;
    private TextView mHourView;
    private TextView mHourSpaceView;
    private TextView mMinuteView;
    private TextView mMinuteSpaceView;
    private TextView mSecondView;
    private TextView mSecondSpaceView;
    private TextView mAmPmTextView;
    private View mAmPmHitspace;
    private RadialPickerLayout mTimePicker;

    private int mSelectedColor;
    private int mUnselectedColor;
    private String mAmText = null;
    private String mPmText = null;

    private boolean mAllowAutoAdvance;
    private Timepoint mInitialTime;
    private boolean mIs24HourMode;
    private Timepoint[] mSelectableTimes;
    private Timepoint mMinTime;
    private Timepoint mMaxTime;
    private boolean mEnableSeconds;

    private String mDoublePlaceholderText;

    /**
     * The callback interface used to indicate the user is done filling in
     * the time (they clicked on the 'Set' button).
     */
    public interface OnTimeSetListener {

        /**
         * @param view      The view associated with this listener.
         * @param hourOfDay The hour that was set.
         * @param minute    The minute that was set.
         * @param second    The second that was set
         */
        void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second);
    }

    public TimePickerDialog() {
        // Empty constructor required for dialog fragment.
    }

    public static TimePickerDialog newInstance(final OnTimeSetListener callback,
                                               final int hourOfDay, final int minute,
                                               final int second, final boolean is24HourMode) {
        TimePickerDialog ret = new TimePickerDialog();
        ret.initialize(callback, hourOfDay, minute, second, is24HourMode);
        return ret;
    }

    public static TimePickerDialog newInstance(final OnTimeSetListener callback,
                                               final int hourOfDay, final int minute,
                                               final boolean is24HourMode) {
        return TimePickerDialog.newInstance(callback, hourOfDay, minute, 0, is24HourMode);
    }

    public void initialize(final OnTimeSetListener callback,
                           final int hourOfDay, final int minute, final int second, final boolean is24HourMode) {
        mCallback = callback;

        mInitialTime = new Timepoint(hourOfDay, minute, second);
        mIs24HourMode = is24HourMode;
        mTitle = "";
        mThemeDark = false;
        mThemeDarkChanged = false;
        mAccentColor = CommonHelper.INVALID;
        mVibrate = true;
        mDismissOnPause = false;
        mEnableSeconds = false;
        mOkResId = R.string.mdtp_ok;
        mCancelResId = R.string.mdtp_cancel;
    }

//--------------------------------------------------------------------------------------------------
//  Lifecycle
//--------------------------------------------------------------------------------------------------

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_INITIAL_TIME)
                && savedInstanceState.containsKey(KEY_IS_24_HOUR_VIEW)) {
            mInitialTime = savedInstanceState.getParcelable(KEY_INITIAL_TIME);
            mIs24HourMode = savedInstanceState.getBoolean(KEY_IS_24_HOUR_VIEW);
            mSelectableTimes = (Timepoint[]) savedInstanceState.getParcelableArray(KEY_SELECTABLE_TIMES);
            mMinTime = savedInstanceState.getParcelable(KEY_MIN_TIME);
            mMaxTime = savedInstanceState.getParcelable(KEY_MAX_TIME);
            mEnableSeconds = savedInstanceState.getBoolean(KEY_ENABLE_SECONDS);
            mAmText = savedInstanceState.getString(KEY_AM);
            mPmText = savedInstanceState.getString(KEY_PM);

            restoreInstanceState(savedInstanceState);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.mdtp_time_picker_dialog, container, false);
        final Activity activity = getActivity();
        final Resources res = getResources();

        // If an accent color has not been set manually, get it from the context
        if (mAccentColor == CommonHelper.INVALID) {
            mAccentColor = DTPickersHelper.getAccentColorFromThemeIfAvailable(activity);
        }
        if (mPrimaryColor == CommonHelper.INVALID) {
            mPrimaryColor = DTPickersHelper.getPrimaryColorFromThemeIfAvailable(activity);
        }

        // if theme mode has not been set by java code, check if it is specified in Style.xml
        if (!mThemeDarkChanged) {
            mThemeDark = DTPickersHelper.isDarkTheme(activity, mThemeDark);
        }

        mSelectedColor = ContextCompat.getColor(activity, R.color.mdtp_white);
        mUnselectedColor = ContextCompat.getColor(activity, R.color.mdtp_accent_color_focused);

        mHourView = view.findViewById(R.id.hours);
        mHourSpaceView = view.findViewById(R.id.hour_space);
        mMinuteSpaceView = view.findViewById(R.id.minutes_space);
        mMinuteView = view.findViewById(R.id.minutes);
        mSecondSpaceView = view.findViewById(R.id.seconds_space);
        mSecondView = view.findViewById(R.id.seconds);
        mAmPmTextView = view.findViewById(R.id.ampm_label);
        if (mAmText == null || mPmText == null) {
            final String[] amPmTexts = new DateFormatSymbols().getAmPmStrings();
            mAmText = amPmTexts[0];
            mPmText = amPmTexts[1];
        }

        mHapticFeedbackController = new HapticFeedbackController(getActivity());
        mInitialTime = roundToNearest(mInitialTime);

        mTimePicker = view.findViewById(R.id.time_picker);
        mTimePicker.setOnValueSelectedListener(this);
        mTimePicker.initialize(getActivity(), this, mInitialTime, mIs24HourMode);

        int currentItemShowing = HOUR_INDEX;
        if (savedInstanceState != null &&
                savedInstanceState.containsKey(KEY_CURRENT_ITEM_SHOWING)) {
            currentItemShowing = savedInstanceState.getInt(KEY_CURRENT_ITEM_SHOWING);
        }
        setCurrentItemShowing(currentItemShowing, false, true);
        mTimePicker.invalidate();

        mHourView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentItemShowing(HOUR_INDEX, true, false);
                tryVibrate();
            }
        });
        mMinuteView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentItemShowing(MINUTE_INDEX, true, false);
                tryVibrate();
            }
        });
        mSecondView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setCurrentItemShowing(SECOND_INDEX, true, false);
                tryVibrate();
            }
        });

        mOkButton = view.findViewById(R.id.ok);
        mOkButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tryVibrate();
                notifyOnDateListener();
                dismiss();
            }
        });
        mOkButton.setTypeface(CommonHelper.getTypeface(activity, CommonHelper.ROBOTO_MEDIUM_FONT));
        if (mOkString != null) {
            mOkButton.setText(mOkString);
        } else {
            mOkButton.setText(mOkResId);
        }

        mCancelButton = view.findViewById(R.id.cancel);
        mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tryVibrate();
                if (getDialog() != null) getDialog().cancel();
            }
        });
        mCancelButton.setTypeface(CommonHelper.getTypeface(activity, CommonHelper.ROBOTO_MEDIUM_FONT));
        if (mCancelString != null) {
            mCancelButton.setText(mCancelString);
        } else {
            mCancelButton.setText(mCancelResId);
        }
        mCancelButton.setVisibility(isCancelable() ? View.VISIBLE : View.GONE);

        // Enable or disable the AM/PM view.
        mAmPmHitspace = view.findViewById(R.id.ampm_hitspace);
        if (mIs24HourMode) {
            mAmPmTextView.setVisibility(View.GONE);
        } else {
            mAmPmTextView.setVisibility(View.VISIBLE);
            updateAmPmDisplay(mInitialTime.isAM() ? CommonHelper.AM : CommonHelper.PM);
            mAmPmHitspace.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Don't do anything if either AM or PM are disabled
                    if (isAmDisabled() || isPmDisabled()) return;

                    tryVibrate();
                    int amOrPm = mTimePicker.getIsCurrentlyAmOrPm();
                    if (amOrPm == CommonHelper.AM) {
                        amOrPm = CommonHelper.PM;
                    } else if (amOrPm == CommonHelper.PM) {
                        amOrPm = CommonHelper.AM;
                    }
                    mTimePicker.setAmOrPm(amOrPm);
                }
            });
        }

        // Disable seconds picker
        if (!mEnableSeconds) {
            mSecondSpaceView.setVisibility(View.GONE);
            view.findViewById(R.id.separator_seconds).setVisibility(View.GONE);
        }

        // Center stuff depending on what's visible
        if (mIs24HourMode && !mEnableSeconds) {
            // center first separator
            RelativeLayout.LayoutParams paramsSeparator = new RelativeLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            paramsSeparator.addRule(RelativeLayout.CENTER_IN_PARENT);
            TextView separatorView = view.findViewById(R.id.separator);
            separatorView.setLayoutParams(paramsSeparator);
        } else if (mEnableSeconds) {
            // link separator to minutes
            final View separator = view.findViewById(R.id.separator);
            RelativeLayout.LayoutParams paramsSeparator = new RelativeLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            paramsSeparator.addRule(RelativeLayout.LEFT_OF, R.id.minutes_space);
            paramsSeparator.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
            separator.setLayoutParams(paramsSeparator);

            if (!mIs24HourMode) {
                // center minutes
                RelativeLayout.LayoutParams paramsMinutes = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                paramsMinutes.addRule(RelativeLayout.CENTER_IN_PARENT);
                mMinuteSpaceView.setLayoutParams(paramsMinutes);
            } else {
                // move minutes to right of center
                RelativeLayout.LayoutParams paramsMinutes = new RelativeLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                paramsMinutes.addRule(RelativeLayout.RIGHT_OF, R.id.center_view);
                mMinuteSpaceView.setLayoutParams(paramsMinutes);
            }
        }

        mAllowAutoAdvance = true;
        setHour(mInitialTime.getHour());
        setMinute(mInitialTime.getMinute());
        setSecond(mInitialTime.getSecond());

        mDoublePlaceholderText = res.getString(R.string.mdtp_time_placeholder);

        // Set the title (if any)
        final TextView timePickerHeader = view.findViewById(R.id.time_picker_header);
        if (!mTitle.isEmpty()) {
            timePickerHeader.setVisibility(TextView.VISIBLE);
            timePickerHeader.setText(mTitle.toUpperCase(Locale.getDefault()));
        }

        // Set the theme at the end so that the initialize()s above don't counteract the theme.
        mOkButton.setTextColor(mAccentColor);
        mCancelButton.setTextColor(mAccentColor);
        timePickerHeader.setBackgroundColor(mPrimaryColor);
        view.findViewById(R.id.time_display_background).setBackgroundColor(mPrimaryColor);
        view.findViewById(R.id.time_display).setBackgroundColor(mPrimaryColor);

        if (getDialog() == null) {
            view.findViewById(R.id.done_background).setVisibility(View.GONE);
        }

        int backgroundColor = ContextCompat.getColor(activity, R.color.mdtp_secondary_light);
        int darkBackgroundColor = ContextCompat.getColor(activity, R.color.mdtp_ternary_light_dark_theme);

        mTimePicker.setBackgroundColor(mThemeDark ? darkBackgroundColor : backgroundColor);
        view.findViewById(R.id.time_picker_dialog).setBackgroundColor(mThemeDark ? darkBackgroundColor : backgroundColor);
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (mTimePicker != null) {
            super.onSaveInstanceState(outState);
            outState.putParcelable(KEY_INITIAL_TIME, mTimePicker.getTime());
            outState.putBoolean(KEY_IS_24_HOUR_VIEW, mIs24HourMode);
            outState.putInt(KEY_CURRENT_ITEM_SHOWING, mTimePicker.getCurrentItemShowing());
            outState.putParcelableArray(KEY_SELECTABLE_TIMES, mSelectableTimes);
            outState.putParcelable(KEY_MIN_TIME, mMinTime);
            outState.putParcelable(KEY_MAX_TIME, mMaxTime);
            outState.putBoolean(KEY_ENABLE_SECONDS, mEnableSeconds);
            outState.putString(KEY_AM, mAmText);
            outState.putString(KEY_PM, mPmText);
        }
    }

//--------------------------------------------------------------------------------------------------
//  Methods
//--------------------------------------------------------------------------------------------------

    private void updateAmPmDisplay(final int amOrPm) {
        if (amOrPm == CommonHelper.AM) {
            mAmPmTextView.setText(mAmText);
        } else if (amOrPm == CommonHelper.PM) {
            mAmPmTextView.setText(mPmText);
        } else {
            mAmPmTextView.setText(mDoublePlaceholderText);
        }
    }

    @Override
    public void advancePicker(final int index) {
        if (!mAllowAutoAdvance) return;
        if (index == HOUR_INDEX) {
            setCurrentItemShowing(MINUTE_INDEX, true, true);
        } else if (index == MINUTE_INDEX && mEnableSeconds) {
            setCurrentItemShowing(SECOND_INDEX, true, true);
        }
    }

    // Show either Hours or Minutes
    private void setCurrentItemShowing(final int index, final boolean animateCircle, final boolean delayLabelAnimate) {
        mTimePicker.setCurrentItemShowing(index, animateCircle);

        TextView labelToAnimate;
        switch (index) {
            case HOUR_INDEX:
                labelToAnimate = mHourView;
                break;
            case MINUTE_INDEX:
                labelToAnimate = mMinuteView;
                break;
            default:
                labelToAnimate = mSecondView;
        }

        final int hourColor = (index == HOUR_INDEX) ? mSelectedColor : mUnselectedColor;
        final int minuteColor = (index == MINUTE_INDEX) ? mSelectedColor : mUnselectedColor;
        final int secondColor = (index == SECOND_INDEX) ? mSelectedColor : mUnselectedColor;
        mHourView.setTextColor(hourColor);
        mMinuteView.setTextColor(minuteColor);
        mSecondView.setTextColor(secondColor);

        final ObjectAnimator pulseAnimator = DTPickersHelper.getPulseAnimator(labelToAnimate, 0.85f, 1.1f);
        if (delayLabelAnimate) {
            pulseAnimator.setStartDelay(PULSE_ANIMATOR_DELAY);
        }
        pulseAnimator.start();
    }

//--------------------------------------------------------------------------------------------------
//
//  Getters & setters
//
//--------------------------------------------------------------------------------------------------

    @Override
    public boolean is24HourMode() {
        return mIs24HourMode;
    }

    /**
     * Set whether an additional picker for seconds should be shown
     *
     * @param enableSeconds true if the seconds picker should be shown
     */
    public void enableSeconds(final boolean enableSeconds) {
        mEnableSeconds = enableSeconds;
    }

    @SuppressWarnings("unused")
    public void setMinTime(final int hour, final int minute, final int second) {
        setMinTime(new Timepoint(hour, minute, second));
    }

    public void setMinTime(final Timepoint minTime) {
        if (mMaxTime != null && minTime.compareTo(mMaxTime) > 0)
            throw new IllegalArgumentException("Minimum time must be smaller than the maximum time");
        mMinTime = minTime;
    }

    @SuppressWarnings("unused")
    public void setMaxTime(final int hour, final int minute, final int second) {
        setMaxTime(new Timepoint(hour, minute, second));
    }

    public void setMaxTime(final Timepoint maxTime) {
        if (mMinTime != null && maxTime.compareTo(mMinTime) < 0)
            throw new IllegalArgumentException("Maximum time must be greater than the minimum time");
        mMaxTime = maxTime;
    }

    @SuppressWarnings("unused")
    public void setSelectableTimes(final Timepoint[] selectableTimes) {
        mSelectableTimes = selectableTimes;
        Arrays.sort(mSelectableTimes);
    }

    /**
     * Set the interval for selectable times in the TimePickerDialog
     * This is a convenience wrapper around setSelectableTimes
     * The interval for all three time components can be set independently
     *
     * @param hourInterval   The interval between 2 selectable hours ([1,24])
     * @param minuteInterval The interval between 2 selectable minutes ([1,60])
     * @param secondInterval The interval between 2 selectable seconds ([1,60])
     */
    public void setTimeInterval(@IntRange(from = 1, to = 24) int hourInterval,
                                @IntRange(from = 1, to = 60) int minuteInterval,
                                @IntRange(from = 1, to = 60) int secondInterval) {
        List<Timepoint> timepoints = new ArrayList<>();

        int hour = 0;
        while (hour < 24) {
            int minute = 0;
            while (minute < 60) {
                int second = 0;
                while (second < 60) {
                    timepoints.add(new Timepoint(hour, minute, second));
                    second += secondInterval;
                }
                minute += minuteInterval;
            }
            hour += hourInterval;
        }
        setSelectableTimes(timepoints.toArray(new Timepoint[timepoints.size()]));
    }

    /**
     * Set the interval for selectable times in the TimePickerDialog
     * This is a convenience wrapper around setSelectableTimes
     * The interval for all three time components can be set independently
     *
     * @param hourInterval   The interval between 2 selectable hours ([1,24])
     * @param minuteInterval The interval between 2 selectable minutes ([1,60])
     */
    public void setTimeInterval(@IntRange(from = 1, to = 24) int hourInterval,
                                @IntRange(from = 1, to = 60) int minuteInterval) {
        setTimeInterval(hourInterval, minuteInterval, 1);
    }

    /**
     * Set the interval for selectable times in the TimePickerDialog
     * This is a convenience wrapper around setSelectableTimes
     * The interval for all three time components can be set independently
     *
     * @param hourInterval The interval between 2 selectable hours ([1,24])
     */
    @SuppressWarnings("unused")
    public void setTimeInterval(@IntRange(from = 1, to = 24) int hourInterval) {
        setTimeInterval(hourInterval, 1);
    }

    public void setOnTimeSetListener(final OnTimeSetListener callback) {
        mCallback = callback;
    }

    public void setStartTime(final int hourOfDay, final int minute, final int second) {
        mInitialTime = roundToNearest(new Timepoint(hourOfDay, minute, second));
    }

    @SuppressWarnings("unused")
    public void setStartTime(final int hourOfDay, final int minute) {
        setStartTime(hourOfDay, minute, 0);
    }

    public boolean isOutOfRange(final Timepoint current) {
        if (mMinTime != null && mMinTime.compareTo(current) > 0) return true;

        if (mMaxTime != null && mMaxTime.compareTo(current) < 0) return true;

        if (mSelectableTimes != null) return !Arrays.asList(mSelectableTimes).contains(current);

        return false;
    }

    @Override
    public boolean isOutOfRange(final Timepoint current, final int index) {
        if (current == null) return false;

        if (index == HOUR_INDEX) {
            if (mMinTime != null && mMinTime.getHour() > current.getHour()) return true;

            if (mMaxTime != null && mMaxTime.getHour() + 1 <= current.getHour()) return true;

            if (mSelectableTimes != null) {
                for (Timepoint t : mSelectableTimes) {
                    if (t.getHour() == current.getHour()) return false;
                }
                return true;
            }

            return false;
        } else if (index == MINUTE_INDEX) {
            if (mMinTime != null) {
                Timepoint roundedMin = new Timepoint(mMinTime.getHour(), mMinTime.getMinute());
                if (roundedMin.compareTo(current) > 0) return true;
            }

            if (mMaxTime != null) {
                Timepoint roundedMax = new Timepoint(mMaxTime.getHour(), mMaxTime.getMinute(), 59);
                if (roundedMax.compareTo(current) < 0) return true;
            }

            if (mSelectableTimes != null) {
                for (Timepoint t : mSelectableTimes) {
                    if (t.getHour() == current.getHour() && t.getMinute() == current.getMinute())
                        return false;
                }
                return true;
            }

            return false;
        } else return isOutOfRange(current);
    }

    @Override
    public boolean isAmDisabled() {
        final Timepoint midday = new Timepoint(12);

        if (mMinTime != null && mMinTime.compareTo(midday) > 0) return true;

        if (mSelectableTimes != null) {
            for (Timepoint t : mSelectableTimes) if (t.compareTo(midday) < 0) return false;
            return true;
        }

        return false;
    }

    @Override
    public boolean isPmDisabled() {
        final Timepoint midday = new Timepoint(12);

        if (mMaxTime != null && mMaxTime.compareTo(midday) < 0) return true;

        if (mSelectableTimes != null) {
            for (Timepoint t : mSelectableTimes) if (t.compareTo(midday) >= 0) return false;
            return true;
        }

        return false;
    }

    @Override
    public String getAmText() {
        return mAmText;
    }

    public void setAmText(final String am) {
        mAmText = am;
    }

    @Override
    public String getPmText() {
        return mPmText;
    }

    public void setPmText(final String pm) {
        mPmText = pm;
    }

    /**
     * Round a given Timepoint to the nearest valid Timepoint
     *
     * @param time Timepoint - The timepoint to round
     * @return Timepoint - The nearest valid Timepoint
     */
    private Timepoint roundToNearest(final Timepoint time) {
        return roundToNearest(time, Timepoint.TYPE.HOUR);
    }

    @Override
    public Timepoint roundToNearest(final Timepoint time, final Timepoint.TYPE type) {
        if (mMinTime != null && mMinTime.compareTo(time) > 0) return mMinTime;

        if (mMaxTime != null && mMaxTime.compareTo(time) < 0) return mMaxTime;
        if (mSelectableTimes != null) {
            int currentDistance = Integer.MAX_VALUE;
            Timepoint output = time;
            for (Timepoint t : mSelectableTimes) {
                if (type == Timepoint.TYPE.MINUTE && t.getHour() != time.getHour()) continue;
                if (type == Timepoint.TYPE.SECOND && t.getHour() != time.getHour() && t.getMinute() != time.getMinute())
                    continue;
                int newDistance = Math.abs(t.compareTo(time));
                if (newDistance < currentDistance) {
                    currentDistance = newDistance;
                    output = t;
                } else break;
            }
            return output;
        }

        return time;
    }

    private void setHour(int value) {
        CharSequence text;
        if (mIs24HourMode) {
            text = CommonHelper.formatIntTwoDig(value, mNumeralSystem);
        } else {
            value = value % 12;
            if (value == 0) {
                value = 12;
            }
            text = CommonHelper.formatInt(value, mNumeralSystem);
        }

        mHourView.setText(text);
        mHourSpaceView.setText(text);
    }

    private void setMinute(int value) {
        if (value == 60) {
            value = 0;
        }
        final CharSequence text = CommonHelper.formatIntTwoDig(value, mNumeralSystem);
        mMinuteView.setText(text);
        mMinuteSpaceView.setText(text);
    }

    private void setSecond(int value) {
        if (value == 60) {
            value = 0;
        }
        final CharSequence text = CommonHelper.formatIntTwoDig(value, mNumeralSystem);
        mSecondView.setText(text);
        mSecondSpaceView.setText(text);
    }

//--------------------------------------------------------------------------------------------------
//  Event handlers
//--------------------------------------------------------------------------------------------------

    public void notifyOnDateListener() {
        if (mCallback != null) {
            mCallback.onTimeSet(mTimePicker, mTimePicker.getHours(), mTimePicker.getMinutes(), mTimePicker.getSeconds());
        }
    }

    /**
     * Called by the picker for updating the header display.
     */
    @Override
    public void onValueSelected(final Timepoint newValue) {
        setHour(newValue.getHour());
        setMinute(newValue.getMinute());
        setSecond(newValue.getSecond());
        if (!mIs24HourMode) updateAmPmDisplay(newValue.isAM() ? CommonHelper.AM : CommonHelper.PM);
    }
}