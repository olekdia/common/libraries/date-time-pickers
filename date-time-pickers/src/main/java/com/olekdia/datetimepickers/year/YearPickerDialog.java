/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.datetimepickers.year;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.olekdia.commonhelpers.CommonHelper;
import com.olekdia.commonhelpers.MathHelper;
import com.olekdia.datetimepickers.DTPickersHelper;
import com.olekdia.datetimepickers.HapticFeedbackController;
import com.olekdia.datetimepickers.IOnDateChangedListener;
import com.olekdia.datetimepickers.PickerDialog;
import com.olekdia.datetimepickers.R;
import com.olekdia.datetimepickers.calendars.CalendarCompat;
import com.olekdia.datetimepickers.date.DateAnimator;

import java.util.Arrays;
import java.util.Calendar;

/**
 * Dialog allowing users to select a month.
 */
public class YearPickerDialog extends PickerDialog implements
        OnClickListener, IYearPickerController {

    private final Calendar mCalendar = CalendarCompat.getInstance();
    private OnDateSetListener mCallBack;

    private DateAnimator mAnimator;
    private boolean mDelayAnimation = true;

    private LinearLayout mYearContainer;
    private TextView mYearView;
    private YearsPickerView mYearsPickerView;
    private int mCurrentView = CommonHelper.INVALID;

    private int mMinYear = MathHelper.roundTo(mCalendar.get(Calendar.YEAR) - 100, 100);
    private int mMaxYear = MathHelper.roundTo(mCalendar.get(Calendar.YEAR) + 200, 100);
    private Calendar mMinDate;
    private Calendar mMaxDate;
    private Calendar[] mHighlightedYears;
    private Calendar[] mSelectableYears;

    /**
     * The callback used to indicate the user is done filling in the date.
     */
    public interface OnDateSetListener {

        /**
         * @param view        The view associated with this listener.
         * @param year        The year that was set.
         */
        void onDateSet(YearPickerDialog view, int year);
    }

    public YearPickerDialog() {
        // Empty constructor required for dialog fragment.
    }

    public void initialize(final OnDateSetListener callBack, final int year) {
        mCallBack = callBack;
        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, Calendar.JANUARY);
        mCalendar.set(Calendar.DAY_OF_MONTH, 1);
    }

//--------------------------------------------------------------------------------------------------
//  Lifecycle
//--------------------------------------------------------------------------------------------------

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Activity activity = getActivity();
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mCurrentView = CommonHelper.INVALID;
        if (savedInstanceState != null) {
            mCalendar.set(Calendar.YEAR, savedInstanceState.getInt(KEY_SELECTED_YEAR));
            mCalendar.set(Calendar.MONTH, Calendar.JANUARY);
            mCalendar.set(Calendar.DAY_OF_MONTH, 1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // All options have been set at this point: round the initial selection if necessary
        setToNearestDate(mCalendar);

        final View view = inflater.inflate(R.layout.mdtp_year_picker_dialog, container, false);

        mYearContainer = (LinearLayout) view.findViewById(R.id.year_picker_year_container);
        mYearContainer.setOnClickListener(this);
        mYearView = (TextView) view.findViewById(R.id.year_picker_year);
        mYearView.setOnClickListener(this);

        int listPosition = CommonHelper.INVALID;
        int listPositionOffset = 0;
        int currentView = YEAR_VIEW;
        if (savedInstanceState != null) {
            mMinYear = savedInstanceState.getInt(KEY_YEAR_START);
            mMaxYear = savedInstanceState.getInt(KEY_YEAR_END);
            currentView = savedInstanceState.getInt(KEY_CURRENT_VIEW);
            listPosition = savedInstanceState.getInt(KEY_LIST_POSITION);
            listPositionOffset = savedInstanceState.getInt(KEY_LIST_POSITION_OFFSET);
            mMinDate = (Calendar) savedInstanceState.getSerializable(KEY_MIN_DATE);
            mMaxDate = (Calendar) savedInstanceState.getSerializable(KEY_MAX_DATE);
            mHighlightedYears = (Calendar[]) savedInstanceState.getSerializable(KEY_HIGHLIGHTED_DAYS);
            mSelectableYears = (Calendar[]) savedInstanceState.getSerializable(KEY_SELECTABLE_DAYS);
            restoreInstanceState(savedInstanceState);
        }

        final Activity activity = getActivity();
        mYearsPickerView = new YearsPickerView(activity, this);

        // if theme mode has not been set by java code, check if it is specified in Style.xml
        if (!mThemeDarkChanged) {
            mThemeDark = DTPickersHelper.isDarkTheme(activity, mThemeDark);
        }

        final int bgColorResource = mThemeDark ? R.color.mdtp_date_picker_view_animator_dark_theme
                : R.color.mdtp_date_picker_view_animator;
        view.setBackgroundColor(ContextCompat.getColor(activity, bgColorResource));

        mAnimator = (DateAnimator) view.findViewById(R.id.animator);
        mAnimator.addView(mYearsPickerView);
        final Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(ANIMATION_DURATION);
        mAnimator.setInAnimation(animation);
        final Animation animation2 = new AlphaAnimation(1.0f, 0.0f);
        animation2.setDuration(ANIMATION_DURATION);
        mAnimator.setOutAnimation(animation2);

        final Button okButton = (Button) view.findViewById(R.id.ok);
        okButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                tryVibrate();
                notifyOnDateListener();
                dismiss();
            }
        });
        okButton.setTypeface(CommonHelper.getTypeface(activity, CommonHelper.ROBOTO_MEDIUM_FONT));
        if (mOkString != null) {
            okButton.setText(mOkString);
        } else {
            okButton.setText(mOkResId);
        }

        final Button cancelButton = (Button) view.findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tryVibrate();
                if (getDialog() != null) getDialog().cancel();
            }
        });
        cancelButton.setTypeface(CommonHelper.getTypeface(activity, CommonHelper.ROBOTO_MEDIUM_FONT));
        if (mCancelString != null) {
            cancelButton.setText(mCancelString);
        } else {
            cancelButton.setText(mCancelResId);
        }
        cancelButton.setVisibility(isCancelable() ? View.VISIBLE : View.GONE);

        // If an accent color has not been set manually, get it from the context
        if (mAccentColor == CommonHelper.INVALID) {
            mAccentColor = DTPickersHelper.getAccentColorFromThemeIfAvailable(activity);
        }
        if (mPrimaryColor == CommonHelper.INVALID) {
            mPrimaryColor = DTPickersHelper.getPrimaryColorFromThemeIfAvailable(activity);
        }
        okButton.setTextColor(mAccentColor);
        cancelButton.setTextColor(mAccentColor);
        view.findViewById(R.id.year_picker_selected_year_layout).setBackgroundColor(mPrimaryColor);

        if (getDialog() == null) {
            view.findViewById(R.id.done_background).setVisibility(View.GONE);
        }

        updateDisplay();
        setCurrentView(currentView);

        if (listPosition != CommonHelper.INVALID) {
            mYearsPickerView.postSetSelectionFromTop(listPosition, listPositionOffset);
        }

        mHapticFeedbackController = new HapticFeedbackController(activity);
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_SELECTED_YEAR, mCalendar.get(Calendar.YEAR));
        outState.putInt(KEY_YEAR_START, mMinYear);
        outState.putInt(KEY_YEAR_END, mMaxYear);
        outState.putInt(KEY_CURRENT_VIEW, mCurrentView);
        outState.putInt(KEY_LIST_POSITION_OFFSET, mYearsPickerView.getFirstPositionOffset());
        outState.putInt(KEY_LIST_POSITION, mYearsPickerView.getFirstVisiblePosition());
        outState.putSerializable(KEY_MIN_DATE, mMinDate);
        outState.putSerializable(KEY_MAX_DATE, mMaxDate);
        outState.putSerializable(KEY_HIGHLIGHTED_DAYS, mHighlightedYears);
        outState.putSerializable(KEY_SELECTABLE_DAYS, mSelectableYears);
    }

//--------------------------------------------------------------------------------------------------
//  Methods
//--------------------------------------------------------------------------------------------------

    private void setCurrentView(final int viewIndex) {
        final ObjectAnimator pulseAnimator = DTPickersHelper.getPulseAnimator(mYearView, 0.85f, 1.1f);
        if (mDelayAnimation) {
            pulseAnimator.setStartDelay(ANIMATION_DELAY);
            mDelayAnimation = false;
        }
        if (mCurrentView != viewIndex) {
            mYearContainer.setSelected(false);
            mYearView.setSelected(true);
            mAnimator.setDisplayedChild(YEAR_VIEW);
            mCurrentView = viewIndex;
        }
        pulseAnimator.start();
    }

    private void updateDisplay() {
        mYearView.setText(CommonHelper.formatInt(mCalendar.get(Calendar.YEAR), mNumeralSystem));
    }

//--------------------------------------------------------------------------------------------------
//  Getters & setters
//--------------------------------------------------------------------------------------------------

    @SuppressWarnings("unused")
    public void setYearRange(final int startYear, final int endYear) {
        if (endYear < startYear) {
            throw new IllegalArgumentException("Year end must be larger than or equal to year start");
        }

        mMinYear = startYear;
        mMaxYear = endYear;
    }

    /**
     * Sets the minimal date supported by this DatePicker. Dates before (but not including) the
     * specified date will be disallowed from being selected.
     *
     * @param calendar a Calendar object set to the year, month, day desired as the mindate.
     */
    @SuppressWarnings("unused")
    public void setMinDate(final Calendar calendar) {
        mMinDate = calendar;
    }

    /**
     * @return The minimal date supported by this DatePicker. Null if it has not been set.
     */
    @SuppressWarnings("unused")
    public Calendar getMinDate() {
        return mMinDate;
    }

    /**
     * Sets the minimal date supported by this DatePicker. Dates after (but not including) the
     * specified date will be disallowed from being selected.
     *
     * @param calendar a Calendar object set to the year, month, day desired as the maxdate.
     */
    @SuppressWarnings("unused")
    public void setMaxDate(Calendar calendar) {
        mMaxDate = calendar;
    }

    /**
     * @return The maximal date supported by this DatePicker. Null if it has not been set.
     */
    @SuppressWarnings("unused")
    public Calendar getMaxDate() {
        return mMaxDate;
    }

    /**
     * Sets an array of dates which should be highlighted when the picker is drawn
     *
     * @param highlightedYears an Array of Calendar objects containing the dates to be highlighted
     */
    @SuppressWarnings("unused")
    public void setHighlightedYears(final Calendar[] highlightedYears) {
        // Sort the array to optimize searching over it later on
        Arrays.sort(highlightedYears);
        mHighlightedYears = highlightedYears;
    }

    /**
     * @return The list of dates, as Calendar Objects, which should be highlighted. null is no dates should be highlighted
     */
    public Calendar[] getHighlightedYears() {
        return mHighlightedYears;
    }

    /**
     * Set's a list of days which are the only valid selections.
     * Setting this value will take precedence over using setMinDate() and setMaxDate()
     *
     * @param selectableYears an Array of Calendar Objects containing the selectable dates
     */
    @SuppressWarnings("unused")
    public void setSelectableYears(final Calendar[] selectableYears) {
        // Sort the array to optimize searching over it later on
        Arrays.sort(selectableYears);
        mSelectableYears = selectableYears;
    }

    /**
     * @return an Array of Calendar objects containing the list with selectable items. null if no restriction is set
     */
    public Calendar[] getSelectableYears() {
        return mSelectableYears;
    }

    @SuppressWarnings("unused")
    public void setOnDateSetListener(final OnDateSetListener listener) {
        mCallBack = listener;
    }

    @Override
    public int getSelectedYear() {
        return mCalendar.get(Calendar.YEAR);
    }

    @Override
    public Calendar getStartDate() {
        if (mSelectableYears != null) return mSelectableYears[0];
        if (mMinDate != null) return mMinDate;
        final Calendar output = Calendar.getInstance();
        output.set(Calendar.YEAR, mMinYear);
        output.set(Calendar.MONTH, Calendar.JANUARY);
        output.set(Calendar.DAY_OF_MONTH, 1);
        return output;
    }

    @Override
    public Calendar getEndDate() {
        if (mSelectableYears != null) return mSelectableYears[mSelectableYears.length - 1];
        if (mMaxDate != null) return mMaxDate;
        final Calendar output = Calendar.getInstance();
        output.set(Calendar.YEAR, mMaxYear);
        output.set(Calendar.MONTH, Calendar.DECEMBER);
        output.set(Calendar.DAY_OF_MONTH, 31);
        return output;
    }

    @Override
    public int getMinYear() {
        if (mSelectableYears != null) return mSelectableYears[0].get(Calendar.YEAR);
        // Ensure no years can be selected outside of the given minimum date
        return mMinDate != null && mMinDate.get(Calendar.YEAR) > mMinYear ? mMinDate.get(Calendar.YEAR) : mMinYear;
    }

    @Override
    public int getMaxYear() {
        if (mSelectableYears != null)
            return mSelectableYears[mSelectableYears.length - 1].get(Calendar.YEAR);
        // Ensure no years can be selected outside of the given maximum date
        return mMaxDate != null && mMaxDate.get(Calendar.YEAR) < mMaxYear ? mMaxDate.get(Calendar.YEAR) : mMaxYear;
    }

    /**
     * @return true if the specified year/month/day are within the selectable days or the range set by minDate and maxDate.
     * If one or either have not been set, they are considered as Integer.MIN_VALUE and
     * Integer.MAX_VALUE.
     */
    @Override
    public boolean isOutOfRange(final int year) {
        if (mSelectableYears != null) {
            return !isSelectable(year);
        }

        if (isBeforeMin(year)) {
            return true;
        } else if (isAfterMax(year)) {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unused")
    public boolean isOutOfRange(final Calendar calendar) {
        return isOutOfRange(calendar.get(Calendar.YEAR));
    }

    private boolean isSelectable(final int year) {
        for (Calendar c : mSelectableYears) {
            if (year < c.get(Calendar.YEAR)) break;
            if (year > c.get(Calendar.YEAR)) continue;
            return true;
        }
        return false;
    }

    private boolean isBeforeMin(final int year) {
        if (mMinDate == null) {
            return false;
        }

        if (year < mMinDate.get(Calendar.YEAR)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isBeforeMin(final Calendar calendar) {
        return isBeforeMin(calendar.get(Calendar.YEAR));
    }

    private boolean isAfterMax(final int year) {
        if (mMaxDate == null) {
            return false;
        }

        if (year > mMaxDate.get(Calendar.YEAR)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isAfterMax(final Calendar calendar) {
        return isAfterMax(calendar.get(Calendar.YEAR));
    }

    private void setToNearestDate(final Calendar calendar) {
        if (mSelectableYears != null) {
            long distance = Long.MAX_VALUE;
            Calendar currentBest = calendar;
            for (Calendar c : mSelectableYears) {
                long newDistance = Math.abs(calendar.getTimeInMillis() - c.getTimeInMillis());
                if (newDistance < distance) {
                    distance = newDistance;
                    currentBest = c;
                } else break;
            }
            calendar.setTimeInMillis(currentBest.getTimeInMillis());
            return;
        }

        if (isBeforeMin(calendar)) {
            calendar.setTimeInMillis(mMinDate.getTimeInMillis());
            return;
        }

        if (isAfterMax(calendar)) {
            calendar.setTimeInMillis(mMaxDate.getTimeInMillis());
            return;
        }
    }

    @Override
    public void registerOnDateChangedListener(IOnDateChangedListener listener) {
    }

    @Override
    public void unregisterOnDateChangedListener(IOnDateChangedListener listener) {
    }

//--------------------------------------------------------------------------------------------------
//  Event handlers
//--------------------------------------------------------------------------------------------------

    public void notifyOnDateListener() {
        if (mCallBack != null) {
            mCallBack.onDateSet(YearPickerDialog.this, mCalendar.get(Calendar.YEAR));
        }
    }

    @Override
    public void onClick(View v) {
        tryVibrate();
        setCurrentView(YEAR_VIEW);
        mYearsPickerView.onDateChanged();
    }

    @Override
    public void onYearSelected(final int year) {
        mCalendar.set(Calendar.YEAR, year);
        updateDisplay();
        if (mAutoDismiss) {
            notifyOnDateListener();
            dismiss();
        }
    }
}