/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.olekdia.datetimepickers.year;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.olekdia.commonhelpers.CommonHelper;
import com.olekdia.datetimepickers.ICommonDatePickerController;
import com.olekdia.datetimepickers.IOnDateChangedListener;
import com.olekdia.datetimepickers.R;
import com.olekdia.datetimepickers.calendars.CalendarCompat;

import java.util.Calendar;

/**
 * Displays a selectable list of years
 */
public class YearsPickerView extends ListView implements OnItemClickListener, IOnDateChangedListener {

    private final ICommonDatePickerController mController;
    private final YearsAdapter mAdapter;
    private final int mViewSize;
    private final int mChildSize;
    private TextViewWithCircularIndicator mSelectedView;
    private final int[] mYearsProvider;
    private final LayoutInflater mInflater;
    private final int mCurrYear;
    private final Typeface mDefNormalTypeface;
    private final Typeface mDefBoldTypeface;

    public YearsPickerView(Context context, ICommonDatePickerController controller) {
        super(context);
        mController = controller;
        mController.registerOnDateChangedListener(this);
        final ViewGroup.LayoutParams frame = new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT,
                                                                        LayoutParams.WRAP_CONTENT);
        setLayoutParams(frame);
        mInflater = LayoutInflater.from(context);
        mDefNormalTypeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL);
        mDefBoldTypeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD);

        final Resources res = context.getResources();
        mViewSize = res.getDimensionPixelOffset(R.dimen.mdtp_date_picker_view_animator_height);
        mChildSize = res.getDimensionPixelOffset(R.dimen.mdtp_year_label_height);
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength(mChildSize / 3);
        mCurrYear = CalendarCompat.getInstance().get(Calendar.YEAR);

        final int fromYear = mController.getMinYear();
        final int toYear = mController.getMaxYear();
        mYearsProvider = new int[toYear - fromYear + 1];
        for (int year = fromYear; year <= toYear; year++) {
            mYearsProvider[year - fromYear] = year;
        }
        mAdapter = new YearsAdapter();
        setAdapter(mAdapter);

        setOnItemClickListener(this);
        setSelector(new StateListDrawable());
        setDividerHeight(0);
        onDateChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mController.tryVibrate();
        TextViewWithCircularIndicator clickedView = (TextViewWithCircularIndicator) view;
        if (clickedView != null) {
            if (clickedView != mSelectedView) {
                if (mSelectedView != null) {
                    mSelectedView.drawIndicator(false);
                    mSelectedView.requestLayout();
                }
                clickedView.drawIndicator(true);
                clickedView.requestLayout();
                mSelectedView = clickedView;
            }
            mController.onYearSelected(getYearFromTextView(clickedView));
            mAdapter.notifyDataSetChanged();
        }
    }

    private static int getYearFromTextView(TextView view) {
        return (int) view.getTag();
    }

    public void postSetSelectionCentered(final int position) {
        postSetSelectionFromTop(position, mViewSize / 2 - mChildSize / 2);
    }

    public void postSetSelectionFromTop(final int position, final int offset) {
        post(new Runnable() {

            @Override
            public void run() {
                setSelectionFromTop(position, offset);
                requestLayout();
            }
        });
    }

    public int getFirstPositionOffset() {
        final View firstChild = getChildAt(0);
        if (firstChild == null) {
            return 0;
        }
        return firstChild.getTop();
    }

    @Override
    public void onDateChanged() {
        mAdapter.notifyDataSetChanged();
        postSetSelectionCentered(mController.getSelectedYear() - mController.getMinYear());
    }

    private class YearsAdapter extends BaseAdapter {

        public YearsAdapter() {
        }

        @Override
        public int getCount() {
            return mYearsProvider.length;
        }

        @Override
        public Object getItem(int position) {
            return mYearsProvider[position];
        }

        public int getYear(int position) {
            return mYearsProvider[position];
        }

        @Override
        public long getItemId(int position) {
            return mYearsProvider[position];
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.mdtp_year_label_text_view, parent, false);
            }

            final TextViewWithCircularIndicator field = (TextViewWithCircularIndicator) convertView;
            final int year = getYear(position);
            field.setText(CommonHelper.formatInt(year, mController.getNumeralSystem()));
            field.setTag(year);
            field.setAccentColor(mController.getAccentColor(), mController.isThemeDark());
            field.requestLayout();
            final boolean selected = mController.getSelectedYear() == year;
            field.drawIndicator(selected);
            if (selected) {
                mSelectedView = field;
            }
            field.setTypeface(mCurrYear == year ? mDefBoldTypeface : mDefNormalTypeface);
            return field;
        }
    }
}