package com.olekdia.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;

import com.olekdia.commonhelpers.CommonHelper;
import com.olekdia.datetimepickers.date.DatePickerDialog;
import com.olekdia.datetimepickers.month.MonthPickerDialog;
import com.olekdia.datetimepickers.time.TimePickerDialog;
import com.olekdia.datetimepickers.year.YearPickerDialog;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class SampleActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String DATE_TAG = "DATE_DLG";

    public static final String TIME_TAG = "TIME_TAG";

    public static final String MONTH_TAG = "MONTH_TAG";

    public static final String YEAR_TAG = "YEAR_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sample);

        findViewById(R.id.time_btn).setOnClickListener(this);
        findViewById(R.id.date_btn).setOnClickListener(this);
        findViewById(R.id.month_btn).setOnClickListener(this);
        findViewById(R.id.year_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.time_btn:
                final TimePickerDialog timeDlg = new TimePickerDialog();
                final Calendar c = GregorianCalendar.getInstance();
                timeDlg.setNumeralSystem(getNumSystem());
                timeDlg.initialize(null, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), 0, DateFormat.is24HourFormat(this));
                timeDlg.show(getSupportFragmentManager(), TIME_TAG);
                break;

            case R.id.date_btn:
                final DatePickerDialog dateDlg = new DatePickerDialog();
                dateDlg.setNumeralSystem(getNumSystem());
                dateDlg.show(getSupportFragmentManager(), DATE_TAG);
                break;

            case R.id.month_btn:
                final MonthPickerDialog monthDlg = new MonthPickerDialog();
                monthDlg.setNumeralSystem(getNumSystem());
                monthDlg.show(getSupportFragmentManager(), MONTH_TAG);
                break;

            case R.id.year_btn:
                final YearPickerDialog yearDlg = new YearPickerDialog();
                yearDlg.setNumeralSystem(getNumSystem());
                yearDlg.show(getSupportFragmentManager(), MONTH_TAG);
                break;
        }
    }

    private int getNumSystem() {
        switch (Locale.getDefault().getLanguage()) {
            case CommonHelper.AR_LANG:
                return CommonHelper.EASTERN_ARABIC;
            case CommonHelper.FA_LANG:
                return CommonHelper.PERSO_ARABIC;
            default:
                return CommonHelper.WESTERN_ARABIC;
        }
    }
}